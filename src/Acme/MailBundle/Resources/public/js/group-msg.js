/**
 * Created by Julia on 14.08.14.
 */
$(document).ready(function () {
    // id for message date: message_date
    console.log("jquery");
    // check senders count
    var select = $("select#message_mailing_sender");
    if (!select.children().length > 0) {
        select.parent("div").append("<span class='help-block'><a href=\""+window.JSON_URLS.emails+"\">" +
            "Добавтьте почтовый аккаунт для рассылки!</a></span> ");
        $("button#message_submit").prop('disabled', true);
    }
    $('#message_text').markItUp(mySettings);
    $("div#message_date_value").hide();
    $("div#template").hide();
    $("div#message_variables").hide();
    $("input#message_use_template").change(function () {
        if ($("#message_use_template").prop("checked")) {
            console.log('checked');
            $("div#template").show();

        } else {
            $("div#template").hide();
            $("#message_text").val("");
            $("div#message_variables").empty();
            $("select#message_template option:first-child").prop("selected", true);
            console.log($("#message_template").val());
        }
    });

    $("select#message_template").change(function () {
        $("#message_text").val("");
        $("div#message_variables").empty();
        $.ajax({
            url: window.JSON_URLS.templates + "/" + $(this).val(),
            type: "GET",
            dataType: "json",
            success: function (data) {
                $("#message_text").val(data.value);
                if (data.variables != undefined) {
                    $("div#message_variables").show();
                    var array = [];
                    for (var j = 0; j < data.variables.length; j++) {
                        if (data.variables[j].type == 1) array.push(data.variables[j].name);
                    }
                }
                var pattern = "<div class='row' id='var'><label for='message_variables___name__'>__label__ : </label>" +
                    "<input type='text' name = 'message[variables][__name__]' id='message_variables___name__'></div>";
                $("div#message_variables").prepend("<label class='control-label' for='var'> Переменные </label>")
                for (var i = 0; i < array.length; i++) {
                    var field = pattern.replace(/__name__/g, i);
                    field = field.replace(/__label__/g, array[i]);
                    console.log(field);
                    $("div#message_variables").append(field);
                    field = "";
                }
                console.log(array);

            }
        });

    });
    $("select#message_date_select").change(function () {
        console.log('changed');
        if ($(this).val() == 1) {
            $("div#message_date_value").hide();
            //get current datetime
            var d = new Date();
            console.log(d);
            var month = parseInt(d.getMonth()) + 1;
            var date = d.getDate() + "-" + month + "-" + d.getFullYear() + " " + d.getHours() + "-" + d.getMinutes() + "-" + d.getSeconds();
            console.log(date);
            $("#message_mailing_date").val(date);
        } else if ($(this).val() == 2) {
            $("div#message_date_value").show();
            $("#message_mailing_date").datepicker({
                dateFormat: 'dd-mm-yy',
                minDate: 1, maxDate: "+1M",
                onSelect: function (datetext) {
                    var d = new Date(); // for now
                    datetext = datetext + " " + d.getHours() + "-" + d.getMinutes() + "-" + d.getSeconds();
                    $('#message_mailing_date').val(datetext);
                }
            });
        }
    });
});

