/**
 * Created by new_name on 14.08.14.
 */
$(document).ready(function() {
    // динамическая загрузка файла и получения ссылки
    var $collectionHolder;
    $collectionHolder = $('div#message_attachments');
    $collectionHolder.data('index', $collectionHolder.find('div#attach').length);

    var prototype_custom = "<div id='attach'>"+
        "<div class='input-group input-group-sm col-md-4'>" +
        "<input class='form-control name' type='text' id = 'message_attachments___name___name'" +
        "name='message[attachments][__name__][name]' placeholder='Имя файла' style = 'z-index:0;'/>" +
        "<span class='input-group-btn'><button class='btn btn-default remove-btn' type='button' id='remove'>x</button></span>"+
        "</div>" +
        "<input class='path' type='hidden' id='message_attachments___name___path' name='message[attachments][__name__][path]'>"+
        "</div>";
    //var prototype = $collectionHolder.data('prototype');
    $("form#upload_file_form").on('submit', fileUpload);

    $("button#add").on('click', function (event) {
        event.preventDefault();
        // get the new index
        var index = $collectionHolder.data('index');

        // Replace '__name__' in the prototype's HTML to
        // instead be a number based on how many items we have
        var form = prototype_custom.replace(/__name__/g, index);
        // launch modal
        $("#upload_modal").modal("show");
        // increase the index with one for the next item
        $collectionHolder.data('index', index + 1);
        $collectionHolder.append(form);
        $("div#attach button#remove").on('click', function(e){
            e.preventDefault();
            console.log("remove att");
            $(this).closest("div#attach").remove();
        });
    });

    function fileUpload(event) {
        event.preventDefault();
        var data = new FormData(this);
        //var data = $(form).serialize();
        console.log(data);
        $.ajax({
            url: window.JSON_URLS.files,
            type: "POST",
            dataType: "json",
            data: data,
            processData: false,
            contentType: false,
            success: function(data){
                console.log(data);
                if(data.status == 'success') {
                    $('div#attach').last().find('input.name').val(data.filename);
                    console.log(data.filename);
                    console.log($('div#attach').last().find('input.name'));
                    $('div#attach').last().children('input.path').val(data.src);
                }
                $("#upload_modal").modal('hide');
            },
            error: function(data){
                $("#upload_modal").modal('hide');
                return false;
            }
        })
    }

    //обратка закрытия окна - удаляем див если не загрузился файл
    $('#upload_modal').on('hidden.bs.modal', function () {
        var attach_div = $('div#attach').last();
        if(attach_div.children('input.path').val()=="")
            attach_div.last().remove();
    });
});
