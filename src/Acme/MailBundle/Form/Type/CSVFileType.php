<?php
namespace Acme\MailBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\File;

class CSVFileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $optins) {
       $builder ->add('name', 'text', array('label' => 'Имя группы:', 'required' => true));
        $builder ->add('divider', 'text', array('label' => 'Разделитель:', 'required' => false, 'max_length' => '4'));
            $builder->add('file', 'file',
                array('constraints' => new File(array(
                        'maxSize' => '200M',
                        'mimeTypes' => array("text/csv", "text/plain"),
                    ))));
        $builder    ->add('target_name', 'text', array('label' => 'Имя:', 'required' => true));
        $builder   ->add('target_surname', 'text', array('label' => 'Фамилия:', 'required' => true));
        $builder->add('target_email', 'text', array('label' => 'Email:', 'required' => true));
        $builder ->add('send', 'submit', array('label' => 'Сохранить'));
    }

    public function setDefaults(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'csfr_protection' => false,
        ));
    }

    public function getName() {
        return 'file_form';
    }
}
