<?php
namespace Acme\MailBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SettingsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', 'text', array('label'=>'Имя', 'required' =>false));
        $builder->add('company', 'text', array('label'=>'Компания', 'required' =>false));
        $builder->add('phone', 'text', array(
            'label'=>'Телефон',
            'required' =>false,
            'max_length' => '11',
            ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Acme\MailBundle\Entity\Settings',
            'cascade_validation' => true
        ));
    }

    public function getName()
    {
        return 'settings';
    }
}