<?php
namespace Acme\MailBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class MessageAPIType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('sender','email', array(
            'label' => 'Отправитель:',
            'required' =>true,
        ));
        $builder->add('target', 'email', array('required' => true, 'label' => 'Получатель'));
        $builder->add('theme', 'text', array('label' => 'Тема письма'));
        $builder->add(
            'text',
            'textarea'

        );
        $builder->add('submit', 'submit',  array('label' => 'Сохранить'));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Acme\MailBundle\Entity\SimpleMessage',
            'cascade_validation' => true,
            'csrf_protection'   => false
        ));
    }

    public function getName()
    {
        return 'api_message';
    }
}