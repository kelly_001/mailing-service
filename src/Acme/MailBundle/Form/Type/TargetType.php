<?php
namespace Acme\MailBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TargetType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder -> add('name', 'text', array('label'=>'Имя', 'required' => false));
        $builder -> add('surname', 'text', array('label'=>'Фамилия', 'required' => false));
        $builder -> add('email', 'email', array('label'=>'Email'));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Acme\MailBundle\Entity\Target',
            'cascade_validation' => 'true'
        ));
    }

    public function getName()
    {
        return 'target';
    }
}