<?php
namespace Acme\MailBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Doctrine\Common\Collections\ArrayCollection;

class GroupMessageType extends AbstractType
{
    private $lists;
    private $senders;
    private $templates;
    function __construct($senders,
                         $lists,
                         $templates ){
        $this->lists = new ArrayCollection();
        $this->lists = $lists;
        $this->senders = $senders;
        $this->templates = $templates;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('mailing', new MailingType($this->lists, $this->senders));
        $builder->add(
            'use_template',
            'checkbox',
            array('property_path' => 'useTemplate', 'required' => false)
        );
        $builder->add('template', 'entity', array(
            'class' => 'AcmeMailBundle:Template',
            'property' => 'name',
            'choices' => $this->templates,
            'label' => '',
            'multiple' => false,
            'expanded' => false,
            'required' =>false,
            'empty_value' => 'Выберите шаблон'
        ));

        $builder->add(
            'text',
            'textarea'
        );

        $builder->add('variables','collection', array(
                'type'=> 'text',
                'label' => 'Переменные шаблона',
                'allow_add' =>true,
                'allow_delete' =>true,
                'required' => false,
                //'prototype' => true,
            ));

        $builder->add('attachments','collection', array(
            'type'=> new AttachmentType(),
            'label' => 'Вложения',
            'allow_add' =>true,
            'allow_delete' =>true,
            'required' => false,
            'prototype' => true,
        ));

        $builder->add('submit', 'submit');
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Acme\MailBundle\Form\Model\GroupMessage',
            'cascade_validation' => 'true'
        ));
    }

    public function getName()
    {
        return 'message';
    }
}