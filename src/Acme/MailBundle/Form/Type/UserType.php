<?php
namespace Acme\MailBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('username', 'email', array('label' => 'Имя пользователя (email)'));
        $builder->add('password', 'repeated', array(
            'first_name'  => 'password',
            'second_name' => 'confirm',
            'type'        => 'password',
            'first_options'  => array('label' => 'Пароль'),
            'second_options' => array('label' => 'Подтверждение пароля'),
        ));
        $builder->add('settings', new SettingsType(),array('required' =>false));
        $builder->add('rate','entity',array(
            'class' => 'AcmeMailBundle:Rate',
            'property' => 'name',
            'label' => 'Тариф:',
            'multiple' => false,
            'expanded' => false,
            'required' =>false
        ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Acme\MailBundle\Entity\User',
            'cascade_validation' => true,
            'csrf_protection'   => false,
        ));
    }

    public function getName()
    {
        return 'user';
    }
}