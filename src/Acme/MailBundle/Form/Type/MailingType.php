<?php
namespace Acme\MailBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Acme\MailBundle\Entity\User;
use Acme\MailBundle\Entity\Email;

class MailingType extends AbstractType
{
    private $lists;
    function __construct($lists, $senders){
        $this->lists = $lists;
        $this->senders = $senders;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('group','entity', array(
            'class' => 'AcmeMailBundle:MailList',
            'property' => 'name',
            'choices' => $this->lists,
            'label' => 'Группа:',
            'multiple' => false,
            'expanded' => false,
            'required' =>true,
        ));

        $builder->add('sender','entity', array(
            'class' => 'AcmeMailBundle:Email',
            'choices' => $this->senders,
            'property' => 'value',
            'label' => 'Отправитель:',
            'multiple' => false,
            'expanded' => false,
            'required' =>true,
        ));

        $builder->add('date','datetime',array(
            'widget'   => 'single_text',
            'label' => 'Время отправки:',
            'input' => 'datetime',
            'format' => 'dd-MM-yyyy HH-mm-ss',
        ));
        $builder->add('theme', 'text');
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Acme\MailBundle\Entity\Mailing',
            'cascade_validation' => true
        ));
    }

    public function getName()
    {
        return 'mailing';
    }
}