<?php
namespace Acme\MailBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class RateType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', 'text', array('label'=>'Название', 'required' =>true));
        $builder->add('description', 'textarea', array('label' => 'Описание'));
        $builder->add('price', 'number', array(
            'label'=>'Стоимость тарифа: ',
            'precision' => 2,
            'required' => true));
        $builder->add('message_price', 'number', array(
            'label'=>'Цена сообщения: ',
            'precision' => 2,
            'required' =>true));
        $builder->add('message_count', 'integer', array('label'=>'Количество:', 'required' =>true));
        $builder->add('submit', 'submit');
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Acme\MailBundle\Entity\Rate',
            'csrf_protection' => false,
        ));
    }

    public function getName()
    {
        return 'rate';
    }
}