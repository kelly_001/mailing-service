<?
namespace Acme\MailBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PayType extends AbstractType {

    public function buildForm (FormBuilderInterface $builder, array $options){
        $builder->add('sum', 'number', array(
            'label'=>'Сумма',
            'precision' => 1,
            'required' =>true));
        $builder->add('payment', 'entity', array(
            'class' => 'AcmeMailBundle:Payment',
            'property' => 'name',
            'label' => 'Способы оплаты',
            'multiple' => false,
            'expanded' => true,
            'required' =>true,
        ));
        $builder->add('submit','submit');
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'cascade_validation' => 'true'
        ));
    }

    public function getName() {
        return 'pay';
    }
}