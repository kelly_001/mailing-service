<?php
namespace Acme\MailBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class MailListType extends AbstractType{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder -> add('name', 'text', array('label'=>'Имя группы:', 'required' =>false));
        $builder ->add('targets', 'collection', array(
            'type' => new TargetType(),
            'allow_add' =>true,
            'allow_delete' =>true,
            'prototype' => true,
            'cascade_validation' => true,
            'by_reference' => false,
        ));
        $builder ->add('submit', 'submit', array('label' => 'Сохранить'));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Acme\MailBundle\Entity\MailList',
            'cascade_validation' => 'true',
            'csrf_protection' =>false,
        ));
    }

    public function getName()
    {
        return 'maillist';
    }
}