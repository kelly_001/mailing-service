<?php
namespace Acme\MailBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class VariableType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder -> add('name', 'text', array('label'=>'', 'required' =>false));
        $builder -> add('type', 'choice', array(
            'label'=>'Тип',
            'required' =>false,
            'choices'   => array('1' => 'Текст пользователя', '2' => 'Стандартное значение'),));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Acme\MailBundle\Entity\Variable',
            'cascade_validation' => true
        ));
    }

    public function getName()
    {
        return 'variable';
    }
}