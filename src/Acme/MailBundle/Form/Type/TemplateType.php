<?php
namespace Acme\MailBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TemplateType extends AbstractType {
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder ->add('name', 'text', array('required' => true, 'label' => 'Название:'));
        $builder ->add('value', 'textarea', array('label' => 'Текст:'));
        $builder ->add('variables', 'collection', array(
            'label' => 'Параметры переменных:',
            'type' => new VariableType(),
            'allow_add' =>true,
            'allow_delete' =>true,
            'prototype' => false,
            'cascade_validation' => true,
            'required' => false
            ));
        $builder ->add('submit', 'submit', array('label' => 'Сохранить'));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Acme\MailBundle\Entity\Template',
            'cascade_validation' => true
        ));
    }

    public function getName()
    {
        return 'template';
    }
}