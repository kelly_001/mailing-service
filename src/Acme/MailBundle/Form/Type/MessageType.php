<?php
namespace Acme\MailBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class MessageType extends AbstractType
{

    private $senders;
    function __construct($senders){
        $this->senders = $senders;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('sender','entity', array(
            'class' => 'AcmeMailBundle:Email',
            'choices' => $this->senders,
            'property' => 'value',
            'label' => 'Отправитель:',
            'multiple' => false,
            'expanded' => false,
            'required' =>true,
        ));
        $builder->add('target', 'email', array('required' => true, 'label' => 'Получатель'));
        $builder->add('theme', 'text', array('label' => 'Тема письма'));
        $builder->add('attachments','collection', array(
            'type'=> new AttachmentType(),
            'label' => 'Вложения',
            'allow_add' =>true,
            'allow_delete' =>true,
            'required' => false,
            'prototype' => true,
        ));
        $builder->add(
            'text',
            'textarea'

        );
        $builder->add('submit', 'submit',  array('label' => 'Сохранить'));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Acme\MailBundle\Entity\SimpleMessage',
            'cascade_validation' => true,
            'csrf_protection'   => false
        ));
    }

    public function getName()
    {
        return 'message';
    }
}