<?php
namespace Acme\MailBundle\Form\Model;

use Symfony\Component\Validator\Constraints as Assert;

use Acme\MailBundle\Entity\User;
use Acme\MailBundle\Entity\Settings;
use Acme\MailBundle\Entity\Rate;

class Registration
{
    /**
     * @Assert\Type(type="Acme\MailBundle\Entity\User")
     * @Assert\Valid()
     */
    protected $user;
    /**
     * @Assert\NotBlank()
     * @Assert\True()
     */
    protected $termsAccepted;

    public function setUser(User $user)
    {
        $this->user = $user;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function getTermsAccepted()
    {
        return $this->termsAccepted;
    }

    public function setTermsAccepted($termsAccepted)
    {
        $this->termsAccepted = (Boolean) $termsAccepted;
    }

}