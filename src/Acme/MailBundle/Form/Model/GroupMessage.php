<?php
namespace Acme\MailBundle\Form\Model;

use Symfony\Component\Validator\Constraints as Assert;
use Acme\MailBundle\Entity\Mailing,
    Acme\MailBundle\Entity\Template,
    Acme\MailBundle\Entity\Message,
    Acme\MailBundle\Entity\User;

use Doctrine\Common\Collections\ArrayCollection;

class GroupMessage
{
    /**
     * @Assert\Type(type="Acme\MailBundle\Entity\Mailing")
     * @Assert\Valid()
     */
    protected $mailing;

    /**
     * @Assert\Type(type="Acme\MailBundle\Entity\Template")
     *
     */
    protected $template;

    /**
     * @Assert\NotBlank()
     */
    protected $text;
    protected $useTemplate;
    protected $variables;
    protected $attachments;

    function __construct() {
        $this->variables = array();
        $this->attachments = new ArrayCollection();
    }

    public function getAttachments() {
        return $this->attachments;
    }

    public function addAttachment(\Acme\MailBundle\Entity\Attachment $vars) {
        $this->attachments[] = $vars;
    }

    public function removeAttachment(\Acme\MailBundle\Entity\Attachment $var) {
        $this->attachments->removeElement($var);
        return $this->attachments;
    }

    public function getVariables() {
        return $this->variables;
    }

    public function addVariable($vars) {
        $this->variables[] = $vars;
    }

    public function removeVariable($var) {
        $this->variables->removeElement($var);
        return $this->variables;
    }

    public function getMailing() {
        return $this->mailing;
    }

    public  function setMailing(Mailing $mailing) {
        $this->mailing = $mailing;
    }

    public function getTemplate() {
        return $this->template;
    }

    public function setTemplate(Template $template) {
        $this->template = $template;
    }

    public function getText() {
        return $this->text;
    }

    public function setText($text){
        $this->text = $text;
    }

    public function getUseTemplate()
    {
        return $this->useTemplate;
    }

    public function setUseTemplate($useTemplate)
    {
        $this->useTemplate = (Boolean) $useTemplate;
    }

}