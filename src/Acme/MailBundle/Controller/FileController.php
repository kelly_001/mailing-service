<?php
namespace Acme\MailBundle\Controller;

use Acme\MailBundle\Entity\Attachment;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Form\FormError;

use Acme\MailBundle\Entity\User,
    Acme\MailBundle\Entity\Template,
    Acme\MailBundle\Entity\MailList,
    Acme\MailBundle\Entity\Target;
use Acme\MailBundle\Form\Type\CSVFileType,
    Acme\MailBundle\Form\Type\AttachmentType;

class FileController extends Controller
{
    public static $FILE_URL = "/uploads/files";
    public static $IMAGE_URL = "/uploads/images";

    public function UploadTemplateAction(Request $request)
    {
        $form = $this->createFormBuilder(null, array(
            'csrf_protection' => false,
            //'validation_constraint' => $collectionConstraint
        ))
            ->add('name', 'text', array('label' => 'Имя', 'required' => true))
            ->add('file', 'file',
                array('constraints' => new File(array(
                        'maxSize' => '30M',
                        'mimeTypes' => array("application/xhtml+xml", "text/html"),
                    ))))
            ->add('send', 'submit', array('label' => 'Сохранить'))
            ->getForm();

        $form->handleRequest($request);

        if ($request->getMethod() == 'POST' && $form->isValid()) {
            $file = $form->get('file')->getData();
            $text = file_get_contents($file);

            $user = $this->get('security.context')->getToken()->getUser();
            $template = new Template();
            $template->setName($form->get('name')->getData());
            $template->setValue($text);

            $template->setUser($user);
            $user->addTemplate($template);
            // create service with template controller class
            $templateService = $this->get('acme.controller.template');
            // send template to parser check
            $template = $templateService->parseTemplate($template);
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            $id = $template->getId();
            return new RedirectResponse($this->generateUrl('_templates', array('id' => $id)));
        }

        return $this->render('AcmeMailBundle:Template:import.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    public function uploadCsvAction(Request $request)
    {
        $logger = $this->get('logger');
        $form = $this->createForm(new CSVFileType());
        $form ->handleRequest($request);

        if ($request->getMethod() == 'POST' && $form->isValid()) {
            $filename = $form->get('file')->getData();
            $divider = $form->get('divider')->getData();
            // параметры контакта -  имя, фамилия, почта
            $properties = array(
                'name' => array(
                    'value'=> $form->get('target_name')->getData(),
                    'index'=> -1,
                ),
                'surname' => array(
                    'value' => $form->get('target_surname')->getData(),
                    'index' => -1,
                ),
                'email' => array(
                    'value' => $form->get('target_email')->getData(),
                    'index' => -1,
                ));
            //проверка на существование и коректность файла
            if (is_file($filename) && file_exists($filename) && (($file = fopen($filename, "r")) !== FALSE)) {
                $logger->info("csv file opened");
                $array = array();
                $file_targets = array();
                    //читываем в массив данные
                    while (($data = @fgetcsv($file, 1000, $divider)) !== FALSE) {
                        // проверка на правильное количество полей, необходимо 3
                        if (count($data) >= 3) $array[] = $data;
                    }

                //получение нужных значений из выгрузки и валидация
                if (is_array($array) && count($array)){
                    for ($i=0; $i<count($array[0]); $i++) {
                        if ($array[0][$i] == $properties['name']['value']) $properties['name']['index'] = $i;
                        if ($array[0][$i] == $properties['surname']['value']) $properties['surname']['index'] = $i;
                        if ($array[0][$i] == $properties['email']['value']) $properties['email']['index'] = $i;
                    }
                    unset($array[0]);

                    foreach ($array as $note) {
                        //email validation
                        $host = "";
                        if (isset($note[$properties['email']['index']]) && $note[$properties['email']['index']]!= "") {
                            $email = $note[$properties['email']['index']];
                            $email_arr = explode("@", $email);
                            if (count($email_arr) == 2) {
                                $host = $email_arr[1];
                                $logger->info("email:".$email."host".$host);
                                if (checkdnsrr($host,"MX")) {
                                    //заполнение нового массива с контактами
                                    $file_targets[] = array(
                                        isset($note[$properties['name']['index']])?$note[$properties['name']['index']]:"",
                                        isset($note[$properties['surname']['index']])?$note[$properties['surname']['index']]:"",
                                        $email,
                                    );
                                }
                            }
                        }
                     }
                } else {
                    $form->addError(new FormError("Ошибка чтения выгрузки"));
                }


                //создаем список рассылки
                if (is_array($file_targets) && count($file_targets)) {
                    $user = $this->get('security.context')->getToken()->getUser();
                    $list = new MailList();
                    $list->setName($form->get('name')->getData());
                    $list->setUser($user);
                    $user->addMailList($list);
                    $targets = array();
                    foreach ($file_targets as $note) {
                        $target = new Target();
                        $target->setName($note[0]);
                        $target->setSurname($note[1]);
                        $target->setEmail($note[2]);
                        $list->addTarget($target);
                        $target->setMaillist($list);
                    }

                    $em = $this->getDoctrine()->getManager();
                    $em->persist($user);
                    $em->flush();
                    return new RedirectResponse($this->generateUrl('_list_home', array('user' => $user)));
                } else {
                    $form->addError(new FormError("Ошибка: нет подходящих записей"));
                }
                fclose($file);
            } else {
                $form->addError(new FormError("Ошибка открытия файла"));
            }
            return $this->render('AcmeMailBundle:List:import.html.twig', array(
                'form' => $form->createView(),
            ));
        }
        return $this->render('AcmeMailBundle:List:import.html.twig', array(
            'form' => $form->createView(),
        ));

    }

    public function uploadImageAction()
    {
        // создаем "на лету" форму  валидатором
        $form = $this->createFormBuilder(null, array(
            'csrf_protection' => false,
            //'validation_constraint' => $collectionConstraint
        ))
            ->add('inlineUploadFile', 'file',
                array('constraints' => new Image(array('mimeTypes' => array("image/png", "image/jpeg", "image/gif")))))
            ->getForm();

        $form->handleRequest($this->get('request'));
        if ($form->isValid()) {
            // обрабатываем файл
            $file = $form->get('inlineUploadFile')->getData();
            $ext = $file->guessExtension();

            if ($ext == '') {
                $response = array(
                    'msg' => 'Какой-то подозрительный файл! Я не хочу его принимать.',
                );
            } else {
                // сохраняем файл
                $uploadDir = $this->get('kernel')->getRootDir() . '/../web/uploads/images';
                $newName = uniqid() . '.' . $ext;
                $file->move($uploadDir, $newName);
                $info = getImageSize($uploadDir . '/' . $newName);

                // возвращаем такой ответ, если все хорошо
                $response = array(
                    'status' => 'success',
                    'src' => $this->container->get('request')->getSchemeAndHttpHost() .
                        $this->container->get('request')->getBasePath() . '/uploads/images/' . $newName,
                    'width' => $info[0],
                    'height' => $info[1],
                );
            }
        } else {
            // возвращаем такой ответ, если все плохо
            $response = array(
                'msg' => 'Your file is not valid!',
            );
        }
        return new Response(json_encode($response));
    }

    public function uploadAttachmentAction(Request $request) {

        // создаем "на лету" форму
        $form = $this->createFormBuilder(null, array(
            'csrf_protection'   => false,
            //'validation_constraint' => $collectionConstraint
        ))
            ->add('name', 'text', array('label' => 'Имя', 'required' => true))
            ->add('inlineUploadFile', 'file',
                array('constraints' =>
                    new File(array(
                        'mimeTypes' => array(
                            "text/html", "text/plain", "text/csv", "text/rtf", "application/pdf",
                            "image/png", "image/jpeg", "image/gif", "image/jpg"))),
                ))
            ->getForm();
        $form->handleRequest($this->get('request'));
        if($form->isValid()) {
            // обрабатываем файл
            $file = $form->get('inlineUploadFile')->getData();
            $ext = $file->guessExtension();
            $name = $form->get('name')->getData() . ".".$ext;

            if ($ext == '') {
                $response = array(
                    'status' => 'error',
                    'msg' => 'Какой-то подозрительный файл! Я не хочу его принимать.',
                );
            } else {
                // сохраняем файл
                $uploadDir = $this->get('kernel')->getRootDir() . '/../web/uploads/files';
                $newName = uniqid() . '.' . $ext;
                $file->move($uploadDir, $newName);

                // возвращаем такой ответ, если все хорошо
                $response = array(
                    'status' => 'success',
                    'src' => $this->container->get('request')->getSchemeAndHttpHost() .
                        $this->container->get('request')->getBasePath() . '/uploads/files/' . $newName,
                    'filename' => $name,
                );
            }
        } else {
            // возвращаем такой ответ, если все плохо
            $response = array(
                'status' => 'error',
                'msg' => 'Your file is not valid!',
            );
        }
        return new Response(json_encode($response));
    }


}