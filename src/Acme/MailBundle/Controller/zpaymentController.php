<?php
namespace Acme\MailBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

use Acme\MailBundle\Entity\PaymentOrder;

class zpaymentController extends Controller
{
    public function checkoutAction(Request $request, $order_id)
    {
        $LMI_PAYEE_PURSE = $this->container->getParameter('zpayment_store');
        $init_pass = $this->container->getParameter('zpayment_init_pass');
        $URL = "https://z-payment.com/merchant.php";

        $order = $this->getDoctrine()
            ->getRepository('AcmeMailBundle:PaymentOrder')
            ->find($order_id);

        $order->setStatus(PaymentOrder::$STATUS_PENDING);
        $em = $this->getDoctrine()->getManager();
        $em->persist($order);
        $em->flush();

        $sign = md5($LMI_PAYEE_PURSE . $order_id . $order->getSum() . $init_pass);
        $fields["LMI_PAYMENT_AMOUNT"]= $order->getSum();
        $fields["LMI_PAYMENT_NO"]= $order->getId();
        $fields["LMI_PAYMENT_DESC"]= "Пополнение счета для " . $order->getUser()->getUsername();
        $fields["CLIENT_MAIL"]= $order->getUser()->getUsername();
        $fields["LMI_PAYEE_PURSE"]= $LMI_PAYEE_PURSE;
        $fields["ZP_SIGN"]= $sign;

        $request_values = array();
        foreach ($fields as $key => $value) {
            $request_values[] = "$key=$value";
        }
        $request_values = implode('&', $request_values);
        $request_url = $URL . "?" . $request_values;
        return new RedirectResponse($request_url);

        /*$form = $this->createFormBuilder(null, array(
            'csrf_protection' => false,
            ))
            ->setAction("https://z-payment.com/merchant.php")
            ->setMethod('POST')
            ->add('LMI_PAYMENT_AMOUNT', 'number', array(
                'data' => $order->getSum()))
            ->add('LMI_PAYMENT_NO', 'integer', array('data' => $order->getId()))
            ->add('LMI_PAYMENT_DESC', 'text', array('data' => "Пополнение счета для " . $order->getUser()->getUsername()))
            ->add('CLIENT_MAIL', 'text', array('data' => $order->getUser()->getUsername()))
            ->add('ZP_SIGN', 'text', array('data' => $sign))
            ->add('LMI_PAYEE_PURSE','integer', array('data' => $LMI_PAYEE_PURSE))
            ->add('submit', 'submit')
            ->getForm();
        return $this->render('AcmeMailBundle:Payment:zpayment.html.twig', array(
            'form'=> $form->createView(),
        ));
        */
    }

    public function resultAction(Request $request){
        $LMI_PAYEE_PURSE = $this->container->getParameter('zpayment_store');
        $merchant_key = $this->container->getParameter('zpayment_merchant_key');

        if($request->getMethod() == 'GET') {
            // $request->get('form[some][deep][data]', default, deep);
            $LMI_PAYEE_PURSE = $request->get('LMI_PAYEE_PURSE', "");
            $LMI_PAYMENT_AMOUNT = $request->get('LMI_PAYMENT_AMOUNT', "");
            $LMI_PAYMENT_NO = $request->get('LMI_PAYMENT_NO', "");
            $LMI_SYS_TRANS_NO = $request->get('LMI_SYS_TRANS_NO', "");
            $LMI_SECRET_KEY = $request->get('LMI_SECRET_KEY', "");
            $LMI_MODE = $request->get('LMI_MODE', "");
            $LMI_SYS_INVS_NO = $request->get('LMI_SYS_INVS_NO', "");
            $LMI_SYS_TRANS_DATE = $request->get('LMI_SYS_TRANS_DATE', "");
            $LMI_PAYER_PURSE = $request->get('LMI_PAYER_PURSE', "");
            $LMI_PAYER_WM = $request->get('LMI_PAYER_WM', "");
            $LMI_HASH = $request->get('LMI_HASH', "");
        } else {
            // $request->request->get('form[some][deep][data]', "");
            $LMI_PAYEE_PURSE = $request->request->get('LMI_PAYEE_PURSE', "");
            $LMI_PAYMENT_AMOUNT = $request->request->get('LMI_PAYMENT_AMOUNT', "");
            $LMI_PAYMENT_NO = $request->request->get('LMI_PAYMENT_NO', "");
            $LMI_SYS_TRANS_NO = $request->request->get('LMI_SYS_TRANS_NO', "");
            $LMI_SECRET_KEY = $request->request->get('LMI_SECRET_KEY', "");
            $LMI_MODE = $request->request->get('LMI_MODE', "");
            $LMI_SYS_INVS_NO = $request->request->get('LMI_SYS_INVS_NO', "");
            $LMI_SYS_TRANS_DATE = $request->request->get('LMI_SYS_TRANS_DATE', "");
            $LMI_PAYER_PURSE = $request->request->get('LMI_PAYER_PURSE', "");
            $LMI_PAYER_WM = $request->request->get('LMI_PAYER_WM', "");
            $LMI_HASH = $request->request->get('LMI_HASH', "");
        }

        //check_hash
        $CalcHash = md5($LMI_PAYEE_PURSE.$LMI_PAYMENT_AMOUNT.$LMI_PAYMENT_NO.$LMI_MODE.$LMI_SYS_INVS_NO.$LMI_SYS_TRANS_NO.$LMI_SYS_TRANS_DATE.$merchant_key.$LMI_PAYER_PURSE.$LMI_PAYER_WM);
        if($LMI_SECRET_KEY==$merchant_key || $LMI_HASH == strtoupper($CalcHash)) {
            // update order and user balance
            $order = $this->getDoctrine()
                ->getRepository('AcmeMailBundle:PaymentOrder')
                ->find($LMI_PAYMENT_NO);
            $user = $order->getUser();
            $user->getSettings()->setBalance($user->getSettings()->getBalance() + $order->getSum());
            $order->setStatus(PaymentOrder::$STATUS_PAID);
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->persist($order);
            $em->flush();
            return new Response("order updated", Response::HTTP_OK);
        } else {
            return new Response("Error: hash check", Response::HTTP_BAD_REQUEST);
        }

    }
}