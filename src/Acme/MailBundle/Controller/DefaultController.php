<?php

namespace Acme\MailBundle\Controller;

use Acme\MailBundle\Entity\User;
use Acme\MailBundle\Entity\Email;
use Acme\MailBundle\Entity\Transport;
use Acme\MailBundle\Entity\Template;
use Acme\MailBundle\Entity\Settings;
use Acme\MailBundle\Entity\Variable;
use Acme\MailBundle\Form\Type\SettingsType;
use Acme\MailBundle\Form\Type\TemplateType;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Doctrine\Common\Collections\ArrayCollection;

use Pagerfanta\Pagerfanta,
    Pagerfanta\Adapter\DoctrineCollectionAdapter;

class DefaultController extends Controller
{
    public function helloAction($message)
    {
        return $this->render('AcmeMailBundle:Default:hello.html.twig', array('message' => $message));
    }

    public function indexAction()
    {
        return $this->render('AcmeMailBundle:Default:index.html.twig');
    }

    public function mailAction(Request $request)
    {
        $messages = array();
        $session = $request->getSession();
        $messages = $session->get('messages');
        return $this->render('AcmeMailBundle:Mail:index.html.twig', array(
            'message' => $messages,
            'user'=>$this->getUser()));
    }

    public function profileAction()
    {
        $templates = $this->getUser()->getTemplates();
        $adapter = new DoctrineCollectionAdapter($templates);
        $pager = new Pagerfanta($adapter);
        $pager->setMaxPerPage(5);

        try {
            $pager->setCurrentPage(1);

        } catch (NotValidCurrentPageException $e) {
            throw new NotFoundHttpException();
        }
        return $this->render('AcmeMailBundle:Default:profile.html.twig', array(
            'user'=>$this->getUser(),
        ));
    }

    public function settingsAction(Request $request)
    {
        $user = $this->getUser();
        $settings = $user->getSettings();
        $message = '';
        $settings_form = $this->createFormBuilder($user, array(
                'cascade_validation' => true
            ))
            ->setMethod('POST')
            ->setAction($this->generateUrl('_profile_settings'))
            ->add('settings', new SettingsType())//, array('cascade_validation' => true))
            ->add('rate', 'entity', array(
                'class' => 'AcmeMailBundle:Rate',
                'property' => 'name',
                'label' => 'Тариф:',
                'multiple' => false,
                'expanded' => false,
                'required' => true))
            ->add('settings_submit', 'submit', array('label' => 'Сохранить'))
            ->getForm();

        if ($request->getMethod() == 'POST') {
            $settings_form->handleRequest($request);
            if ($settings_form->isValid()){
                $settings = $settings_form->get('settings')->getData();
                    $user->setRate($settings_form->get('rate')->getData());
                    //$user->setSettings($settings);
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($user);
                    $em->flush();
                    $message = 'Settings saved';
                }
        }

        return $this->render('AcmeMailBundle:Default:settings.html.twig', array(
            'settings_form' => $settings_form->createView(),
            'message' => $message));
    }

    public function rateAction(Request $request)
    {
        $user = $this->getUser();
        $rate_form = $this->createFormBuilder()
            ->setAction($this->generateUrl('_profile_rate'))
            ->setMethod('POST')
            ->add('rate', 'entity', array(
                'class' => 'AcmeMailBundle:Rate',
                'property' => 'name',
                'label' => 'Тариф:',
                'multiple' => false,
                'expanded' => false,
                'required' => false))
            ->add('rate_submit', 'submit', array('label' => 'Сохранить'))
            ->getForm();
        $message = "";
        if ($request->getMethod() == 'POST') {
            $rate_form->handleRequest($request);
            if ($rate_form->isValid()) {
                $user->setRate($rate_form->get('rate')->getData());
                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();
                return new RedirectResponse($this->generateUrl('_profile'));
            }
        }
        return $this->render('AcmeMailBundle:Default:rate.html.twig', array(
            'rate_form' => $rate_form->createView(),
            'message' => $message));
    }


    public function emailAction(Request $request, $page, $id)
    {
        $user = $this->getUser();
        // проверка - новый емеил или редактирование существующего
        if($id != 0){ // редактирование
            $email = $this->getDoctrine()
                ->getRepository('AcmeMailBundle:Email')
                ->find($id);
            if(!$email || $email->getUser()->getId() != $user->getId())return new Response("wrong email id", 401);
        } else {    //создаем новый
            $email = new Email();
        }
        // форма дляя редактирования
        $form = $this->createFormBuilder($email)
            ->add('value', 'email', array('required' => true, 'label' => 'Имя почты'))
            ->add('password', 'repeated', array(
                'first_name'  => 'password',
                'second_name' => 'confirm',
                'type'        => 'password',
                'first_options'  => array('label' => 'Пароль'),
                'second_options' => array('label' => 'Подтверждение пароля'),
            ))
            ->add('submit','submit')
            ->getForm();
        if ($request->getMethod() == 'POST') {
            $form->bind($request);
            if ($form->isValid()) {
               //проверка и поиск настроек транспорта для отправки
                $transport = $this->checkTransport($email->getValue());
                if ($transport != false) {
                    $email->setTransport($transport);
                    // проверка - новый емейл или редактирование существующего
                    if ($id == 0) {
                        $email->setStatus(Email::$STATUS_NEW);
                        $email->setUser($user);
                        $user->addEmail($email);
                    }

                    $em = $this->getDoctrine()->getManager();
                    $em->persist($user);
                    $em->flush();
                    //send confirm letter
                    $text_view = $this->renderView('AcmeMailBundle:Templates:confirm_email.html.twig', array(
                        'url' => $this->container->get('request')->getSchemeAndHttpHost().
                            //$this->container->get('router')->getContext()->getBaseUrl().
                            $this->generateUrl('_emails_confirm', array('id'=>$email->getId()))
                    ));
                    $this->sendMail($this->container->getParameter('mailer_user'), $email->getValue(), "Email confirmation", $text_view);

                    return new RedirectResponse($this->generateUrl('_emails'));
                } else {
                    $form->addError(new FormError("Не найдено настроек транспорта")); //TODO set new transport
                }
            }
        }
        // Pagination
        if(!$page) $page = 1;
        $emails = $user->getEmails();
        $adapter = new DoctrineCollectionAdapter($emails);
        $pager = new Pagerfanta($adapter);
        $pager->setMaxPerPage(5);
        try {
            $pager->setCurrentPage($page);

        } catch (NotValidCurrentPageException $e) {
            throw new NotFoundHttpException();
        }
        return $this->render('AcmeMailBundle:Default:email.html.twig', array(
            'form' => $form->createView(),
            'pager' => $pager,
            'id' => $id
            ));
    }

    public function confirmEmailAction(Request $request, $id){
        $email = $this->getDoctrine()
            ->getRepository('AcmeMailBundle:Email')
            ->find($id);
        if(!$email) throw $this->createNotFoundException('not found');
        if ($email->getStatus() == Email::$STATUS_NEW)
            $email->setStatus(Email::$STATUS_CHECKED);
        elseif ($email->getStatus() == Email::$STATUS_CHECKED)
            $email->setStatus(Email::$STATUS_ACTIVE);
        $em = $this->getDoctrine()->getManager();
        $em->persist($email);
        $em->flush();

        return $this->render('AcmeMailBundle:Default:hello.html.twig', array(
            'message' => 'Email подтвержден'
        ));
    }

    public function removeEmailAction(Request $request, $id){
        $email = $this->getDoctrine()
            ->getRepository('AcmeMailBundle:Email')
            ->find($id);

        $user = $this->get('security.context')->getToken()->getUser();
        if(!$email || $email->getUser()->getId() != $user->getId())return new Response("wrong email id", 401);
        $mailings = $email -> getMailings();
        foreach ($mailings as $mailing) {
            $email->removeMailing($mailing);
            $mailing->setSender(null);
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($email);
        $em->flush();
        return new RedirectResponse($this->generateUrl('_profile'));
    }

    public function getUser()
    {
        return $this->get('security.context')->getToken()->getUser();
    }

    public function sendMail($from, $target, $theme, $text)
    {
        $message = \Swift_Message::newInstance()
            ->setSubject($theme)
            //->setFrom($from)
            ->setSender($from)
            ->setTo($target)
            ->setBody($text, 'text/html');
        $result = $this->get('mailer')->send($message);
        if ($result > 0) return true;
        else return false;
    }

    private function checkTransport($email) {
        $domain_pattern = '/@\w+\.\w+$/';
        if (preg_match($domain_pattern, $email, $match))
            $domain = $match[0];
        // search for transport
        $transports = $this->getDoctrine()->getRepository('AcmeMailBundle:Transport')->findAll();
        foreach ($transports as $transport) {
            $domains = explode(';', $transport->getDomains());
            if(in_array($domain,$domains))  return $transport;
        }
        return false;
    }

}
