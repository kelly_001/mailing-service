<?php
namespace Acme\MailBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use Acme\MailBundle\Entity\MailList;
use Acme\MailBundle\Entity\Target;
use Acme\MailBundle\Entity\User;
use Acme\MailBundle\Form\Type\MailListType;

use Pagerfanta\Pagerfanta,
    Pagerfanta\Adapter\DoctrineCollectionAdapter;

class ListController extends Controller
{

    public function listsAction($page)
    {
        $lists = $this->getUser()->getMaillists();
        $adapter = new DoctrineCollectionAdapter($lists);
        $pager = new Pagerfanta($adapter);
        $pager->setMaxPerPage(5);

        try {
            $pager->setCurrentPage($page);

        } catch (NotValidCurrentPageException $e) {
            throw new NotFoundHttpException();
        }

        return $this->render('AcmeMailBundle:List:index.html.twig', array('pager'=> $pager));
    }

    public function manageAction(Request $request, $id)
    {
        $user = $this->get('security.context')->getToken()->getUser();
        if ($id != 0) {
            $list = $this->getDoctrine()
                ->getRepository('AcmeMailBundle:MailList')
                ->find($id);
            if(!$list || $list->getUser()->getId() != $user->getId())return new Response("wrong mailing id", 401);
        } else {
            $list = new MailList();
            $list->addTarget(new Target());
        };
        $original = new ArrayCollection();
        foreach ($list->getTargets() as $target) {
            $original->add($target);
        }
        $form = $this->createForm(new MailListType(), $list);
        if ($request->getMethod() == "POST") {
            $form->handleRequest($request);
            if ($form->isValid()) {
                if ($id == 0) {
                    $list->setUser($user);
                    $user->addMailList($list);
                }
                $em = $this->getDoctrine()->getManager();
                foreach ($original as $target) {
                    if (false === $list->getTargets()->contains($target)) {
                        // remove the Task from the Tag
                        $list->getTargets()->removeElement($target);
                        // if it was a many-to-one relationship, remove the relationship like this
                        $target->setMaillist(null);
                        $em->remove($target);
                    }
                }

                $em->persist($user);
                foreach ($list->getTargets()  as $target) {
                    $target->setMaillist($list);
                    $em->persist($target);
                }
                $em->flush();
                return new RedirectResponse($this->generateUrl('_list_home'));
            } else {
                return $this->render('AcmeMailBundle:List:manage.html.twig', array(
                    'form' => $form->createView(),
                    'id' => $id,
                ));
            }
        } else {
            return $this->render('AcmeMailBundle:List:manage.html.twig', array(
                'form' => $form->createView(),
                'id' => $id,
            ));
        }
    }

    public function removeAction(Request $request, $id)
    {
        $list = $this->getDoctrine()
            ->getRepository('AcmeMailBundle:MailList')
            ->find($id);

        $user = $this->get('security.context')->getToken()->getUser();
        if(!$list || $list->getUser()->getId() != $user->getId())return new Response("wrong mailing id", 401);
        //$user->removeMaillist();
        $list ->getMailings()->removeElement($list);
        $em = $this->getDoctrine()->getManager();
        $em->remove($list);
        $em->flush();
        return new RedirectResponse($this->generateUrl('_list_home'));
    }

    public function getUser()
    {
        return $this->get('security.context')->getToken()->getUser();
    }
}