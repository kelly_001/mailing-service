<?php

namespace Acme\MailBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Form\FormError;

use Acme\MailBundle\Entity\MailList,
    Acme\MailBundle\Entity\Mailing,
    Acme\MailBundle\Entity\SimpleMessage,
    Acme\MailBundle\Entity\Message,
    Acme\MailBundle\Entity\Target,
    Acme\MailBundle\Entity\User,
    Acme\MailBundle\Entity\Variable,
    Acme\MailBundle\Entity\Email,
    Acme\MailBundle\Entity\Attachment,
    Acme\MailBundle\Entity\Template;

use Acme\MailBundle\Form\Type\GroupMessageType,
    Acme\MailBundle\Form\Model\GroupMessage,
    Acme\MailBundle\Form\Type\MessageType;

use Symfony\Component\HttpFoundation\Session\Session;

class MailController extends Controller
{
    public static $STATUS_NEW = 1;
    public static $STATUS_PREPARE = 2;
    public static $STATUS_SENT = 3;
    public static $STATUS_ERROR = 4;
    public static $STATUS_NEED_PAY = 5;
    public static $STATUS_CANCELLED = 6;
    public static $STATUS_VIEWED = 7;

    public function groupMessageAction(Request $request)
    {
        $error_msg = array(
            "msg" => "",
            "link" => "",
        );
        $logger = $this->get('logger');
        $user = $this->get('security.context')->getToken()->getUser();
        $message_view = new GroupMessage();
        $senders = array();
        foreach ($user->getEmails() as $email) {
            if ($email->getStatus() == 3 and $email->getTransport() !== null)
                $senders[] = $email;
        }
        $form = $this->createForm(new GroupMessageType(
            $senders, $user->getMailLists(), $user->getTemplates()), $message_view);
        $form->handleRequest($request);
        if ($request->getMethod() == 'POST') {
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                //save to db: 1 step - mailing entity, 2 step: messages
                $message_view = $form->getData();
                $mailing = $message_view ->getMailing();
                $mailing ->setStatus(Mailing::$STATUS_NEW);
                $user->addMailing($mailing);
                    $mailing->setUser($user);

                // проверяем, хватает ли денег на балансе
                $targets = $mailing->getGroup()->getTargets();
                if (!is_null($user->getRate())) {
                    $balance = $user->getSettings()->getBalance();
                    $new_balance = $balance - ($targets->count() * $user->getRate()->getMessagePrice());
                    if ($balance <= 0 or $new_balance < 0) {
                        $mailing->setStatus(Mailing::$STATUS_NEED_PAY);
                        // TODO notice user
                        $error_msg['link'] = $this->generateUrl('_profile_pay');
                        $error_msg['msg'] = "Недостаточно средств на балансе!";
                            //"<a href='$link' target='_blank'>Пополнить баланс</a>";
                    } else $user->getSettings()->setBalance($new_balance);
                } else {
                    $mailing->setStatus(Mailing::$STATUS_NEED_PAY);
                    // TODO notice user, redirect to rate page
                    $error_msg['link'] = $this->generateUrl('_profile_rate');
                    $error_msg['msg'] = "Нужно выбрать тариф!";
                        //"<a href='$link' target='_blank'>Тарифы</a>";
                }

                $attachments = $message_view->getAttachments();
                //get & save attachments
                if (count($attachments)) {
                    foreach($attachments as $attachment) {
                        $mailing->addAttachment($attachment);
                            $attachment->setMailing($mailing);
                        $em->persist($attachment);
                    }
                }
                $em->persist($user);
                //$em->flush();


                //foreach target from maillist create message
                foreach ($targets as $target) {
                    // step 1: create&save Entity
                    $message = new Message();
                    $message->setStatus(Message::$STATUS_NEW);
                    $message->setMailing($mailing);
                        $mailing->addMessage($message);
                    $message->setTarget($target);
                        $target->addMessage($message);
                    $message->setDate($mailing->getDate());
                    if (!is_null($user->getRate())) $message->setPrice($user->getRate()->getMessagePrice());
                    $message ->setText($message_view->getText()); //plain text

                    //$em->persist($message);
                    //$em->flush();

                    // Message text render
                    if ($message_view->getUseTemplate()){
                        $vars = $message_view ->getTemplate()->getVariables();
                        $values = $message_view->getVariables();

                        $index = 0;
                        $fields = array();
                        foreach ($vars as $var) {
                            if ($var->getType() == Template::$TYPE_TEXT) {
                                // plain text var
                                $fields[$var->getName()] = isset($values[$index])?$values[$index]:"";
                                $index += 1;
                            } elseif ($var->getType() == Template::$TYPE_VAR) {
                                // entity var
                                $name = $var->getName();
                                switch ($name){
                                    case "message":
                                        $fields[$name] = $message_view->getText();
                                        break;
                                    case "user":
                                        $fields[$name] = is_null($target->getName())?"":$target->getName(). " ".
                                        is_null($target->getSurname())?"":$target->getSurname();
                                        break;
                                    case "user_email":
                                        $fields[$name] = $target->getEmail();
                                        break;
                                    case "theme":
                                        $fields[$name] = $mailing->getTheme();
                                        break;
                                    case "sender_email":
                                        $fields[$name] = is_null($mailing->getSender())?"":$mailing->getSender()->getValue();
                                        break;
                                    case "link":
                                        $fields[$name] =
                                            $this->container->get('request')->getSchemeAndHttpHost().
                                            $this->container->get('router')->getContext()->getBaseUrl().
                                            "/mail/view/".$message->getId();
                                        break;
                                    default:
                                        $fields[$name] = "";
                                }
                            }
                        }
                        /*
                         * вызываем функцию из Template Controller для замены шаблона переменных
                         * с пользовательских [[]] на твиг {{}}
                         */
                        // create service with template controller class
                        $templateService = $this->get('acme.controller.template');
                        // send template to parser check
                        $text = $templateService->prerenderTemplate($message_view->getText(),$message_view ->getTemplate()->getVariables() );
                        $text_view = $this->get('mine.twig_string')->render($text, $fields);
                        $message->setText($text_view);
                    } else {
                        $template = $this->getFile(__DIR__.'/../Resources/views/Templates/plain_message.html.twig');
                        $message ->setText(
                            $this->get('mine.twig_string')->render($template,
                                array('message'=> $message_view->getText(), 'theme' =>$mailing->getTheme()))
                        );
                    }
                    $em->persist($message);
                    }
                $em->flush();
                $session = $request->getSession();

                $session->set('messages', $error_msg);
                return new RedirectResponse($this->generateUrl('_mail_home'));
            } else {
                return $this->render('AcmeMailBundle:Mail:group_mail.html.twig', array(
                    'form' => $form->createView(),
                    'user' => $user
                ));
            }
        } else {
            return $this->render('AcmeMailBundle:Mail:group_mail.html.twig', array(
                'form' => $form->createView(),
                'user' => $user
            ));
        }
    }

    public function simpleMessageAction(Request $request)
    {
        $user= $this->get('security.context')->getToken()->getUser();
        $senders = array();
        foreach ($user->getEmails() as $email) {
            if ($email->getStatus() == 3 and $email->getTransport() !== null)
                $senders[] = $email;
        }

        $message = new SimpleMessage();
        $form = $this->createForm(new MessageType($senders), $message);

        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            //var_dump($form->getData());
            if ($form->isValid()) {
                $logger = "";
                // получаем данные с формы
                $sender = $form->get('sender')->getData();
                $target = $form->get("target")->getData();
                $theme = $form->get("theme")->getData();
                $text = $form->get('text')->getData();

                //создаем и сохраняем новое сообщение
                $message->setStatus(Message::$STATUS_NEW);
                $message->setUser($user);
                $user->addMessage($message);
                $sender->addMessage($message);
                /* цена = 0, бесплатные простые сообщения
                / дата сообщения - сейчас
                / date_default_timezone_set('Asia/Irkutsk');
                / $now = new \DateTime("now"); //date('Y-m-d H-i', time());
                */
                $attachments = $form->get('attachments')->getData();
                if (count($attachments)) {
                    foreach($attachments as $attachment) {
                        $message->addAttachment($attachment);
                        $attachment ->setSimpleMessage($message);
                    }
                }
                $template = $this->getFile(__DIR__.'/../Resources/views/Templates/plain_message.html.twig');
                $message ->setText(
                    $this->get('mine.twig_string')->render($template,
                        array('message'=> $text, 'theme' =>$theme))
                );
                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();
                //echo $message;
                return $this->render('AcmeMailBundle:Mail:index.html.twig');
            }
        }

            return $this->render('AcmeMailBundle:Mail:mailform.html.twig', array(
                'form' => $form->createView(),
                'user' => $user,
            ));

    }

    public function testmailAction(Request $request)
    {
        $message = "";
        if ($this->sendMail("kelly_001@mail.ru", "kelly_001@mail.ru", "Test symfony mailing", "Dear user!")) $message = 'success';
        else $message = 'fail';
        return $this->render('AcmeMailBundle:Mail:index.html.twig');
    }


    public function sendMail($from, $target, $theme, $text)
    {
        /*$text_view = $this->renderView('AcmeMailBundle:Templates:plain_message.html.twig', array(
            'username' => $target,
            'theme' => $theme));*/
        $message = \Swift_Message::newInstance()
            ->setSubject($theme)
            //->setFrom($from)
            ->setSender($from)
            ->setTo($target)
            ->setBody($text, 'text/html');
        $result = $this->get('mailer')->send($message);
        if ($result > 0) return true;
        else return false;
    }

    //MAILBOX

    public function MailSentAction($type, $page) {
        $user = $this->get('security.context')->getToken()->getUser();

        date_default_timezone_set('Asia/Irkutsk');
        $now = new \DateTime("now"); //date('Y-m-d H-i', time());

        $repository = $this->getDoctrine()->getRepository('AcmeMailBundle:Mailing');
        $em = $this->getDoctrine()->getManager();

        $limit = 5;
        $offset = ($page-1) * $limit;
        if ($type == "sent") {
            $query = $repository ->createQueryBuilder('m')
                ->where('(m.status = :sent OR m.status = :dost OR m.status = :cancelled)  AND m.user = :id')
                ->add('orderBy','m.status ASC, m.date ASC')
                ->setParameters(array(
                    'sent' => Mailing::$STATUS_SENT,
                    'cancelled' => Mailing::$STATUS_CANCELLED,
                    'dost' =>  Mailing::$STATUS_DOSTAVLENO,
                    'id' => $user->getId()))
                ->setFirstResult( $offset )
                ->setMaxResults( $limit )
                ->getQuery();
            $total_query = $em->createQuery(
                "SELECT count(m) FROM AcmeMailBundle:Mailing m
                 WHERE (m.status = :sent OR m.status = :dost OR m.status = :cancelled) AND m.user = :id")
                ->setParameters(array(
                    'sent' => Mailing::$STATUS_SENT,
                    'cancelled' => Mailing::$STATUS_CANCELLED,
                    'dost' =>  Mailing::$STATUS_DOSTAVLENO,
                    'id' => $user->getId()
                ));
        } else if ($type == "que") {
            $query = $repository ->createQueryBuilder('m')
                ->where('(m.status != :sent AND m.status != :dost AND m.status != :cancelled) AND m.user = :id')
                ->setParameters(array(
                    'sent' => Mailing::$STATUS_SENT,
                    'cancelled' => Mailing::$STATUS_CANCELLED,
                    'dost' =>  Mailing::$STATUS_DOSTAVLENO,
                    'id' => $user->getId()
                ))
                ->add('orderBy','m.status ASC, m.date ASC')
                ->setFirstResult( $offset )
                ->setMaxResults( $limit )
                ->getQuery();
            $total_query = $em->createQuery("SELECT count(m) FROM AcmeMailBundle:Mailing m
                    WHERE (m.status != :sent AND m.status != :dost AND m.status != :cancelled) AND m.user = :id")
                    ->setParameters(array(
                        'sent' => Mailing::$STATUS_SENT,
                        'cancelled' => Mailing::$STATUS_CANCELLED,
                        'dost' =>  Mailing::$STATUS_DOSTAVLENO,
                        'id' => $user->getId()
                    ));
        } else {
            $query = $repository ->createQueryBuilder('m')
                ->where('m.user = :id')
                ->setParameters(array('id' => $user->getId()))
                ->orderBy('m.status', 'ASC')
                ->orderBy('m.date', 'ASC')
                ->setFirstResult( $offset )
                ->setMaxResults( $limit )
                ->getQuery();
            $total_query = $em->createQuery(
                "SELECT count(m) FROM AcmeMailBundle:Mailing m WHERE m.user = :id")
                ->setParameters(array('id' => $user->getId()));
        }
        $total = $total_query->getSingleScalarResult();
        $mails = $query->getResult();
        if(!$mails) return new RedirectResponse($this->generateUrl('_mail_home'));

        return $this->render('AcmeMailBundle:Mail:mailbox.html.twig', array(
            'mails' =>$mails,
            'page' => $page,
            'mails_size' => $total,
            'type' => $type,
            'limit' => $limit,
        ));
    }

    public function CancelMailAction($id) {
        $mailing = $this->getDoctrine()
            ->getRepository('AcmeMailBundle:Mailing')
            ->find($id);
        if(!$mailing) throw $this->createNotFoundException('not found');
        $em = $this->getDoctrine()->getManager();

        $mailing->setStatus(Mailing::$STATUS_CANCELLED);
        $em->persist($mailing);
        $em->flush();
        return new RedirectResponse($this->generateUrl('_mail_mail_sent', array('type' => "sent")));
    }

    public function MailViewAction(Request $request, $id) {
        $message = $this->getDoctrine()
            ->getRepository('AcmeMailBundle:Message')
            ->find($id);
        if(!$message) throw $this->createNotFoundException('not found');
        $text = $message->getText();
        $view = $this->get('mine.twig_string')->render($text);
        //echo($view);
        return new Response($this->get('mine.twig_string')->render($text));
    }

    public function MailUserViewAction(Request $request, $id) {
        $message = $this->getDoctrine()
            ->getRepository('AcmeMailBundle:Message')
            ->find($id);
        if(!$message) throw $this->createNotFoundException('not found');

        $message->setStatus(Message::$STATUS_VIEWED);
        $em = $this->getDoctrine()->getManager();
        $em->persist($message);
        $em->flush();

        $text = $message->getText();
        $view = $this->get('mine.twig_string')->render($text);
        //echo($view);
        return new Response($this->get('mine.twig_string')->render($text));
    }


    public function resendMailAction(Request $request) {
        date_default_timezone_set('Asia/Irkutsk');
        $now = new \DateTime("now"); //date('Y-m-d H-i', time());

        $user = $this->get('security.context')->getToken()->getUser();
        $request = $this->get('request');

            $id = $request->request->get('id');
            if ($id != 0) {
                $date = $request->request->get('date');
                $date = new \DateTime($date);
                if (is_null($date) or !isset($date)) $date = $now;
                $mailing = $this->getDoctrine()
                    ->getRepository('AcmeMailBundle:Mailing')
                    ->find($id);
                if(!$mailing) throw $this->createNotFoundException('not found');

                $status = Mailing::$STATUS_NEW;
                $count = $mailing->getMessages();
                foreach ($mailing->getMessages() as $msg) {
                    if (!is_null($user->getRate())) {
                        $balance = $user->getSettings()->getBalance();
                        $new_balance = $balance - ($count * $user->getRate()->getMessagePrice());
                        if ($balance <= 0 or $new_balance < 0) {
                            $mailing->setStatus(Mailing::$STATUS_NEED_PAY);
                            // TODO notice user
                        } else $user->getSettings()->setBalance($new_balance);
                    } else {
                        $mailing->setStatus(Mailing::$STATUS_NEED_PAY);
                        // TODO notice user, redirect to rate page
                    }
                    $msg -> setStatus($status);
                    $msg -> setDate($date);
                }
                $mailing -> setStatus($status);
                $mailing ->setDate($date);

                $em = $this->getDoctrine()->getManager();
                $em->persist($mailing);
                $em->flush();

                return new Response("Saved", Responce::HTTP_OK);

            } else return new Response("Error: id null", Responce::HTTP_BAD_REQUEST);
        }

    private function getFile($path){
        try {
            $handle = fopen($path, "r");
            $result = "";
            if ($handle) {
                while (($buffer = fgets($handle, 4096)) !== false) {
                    $result .= $buffer;
                }
                if (!feof($handle)) {
                    return "Error: unexpected fgets() fail\n";
                }
                fclose($handle);
            }
            return $result;
        } catch (Exception $error)    {
            return null;
        }
    }

}