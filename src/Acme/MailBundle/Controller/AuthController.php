<?php

namespace Acme\MailBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Acme\MailBundle\Entity\Settings;
use Acme\MailBundle\Entity\User;
use Acme\MailBundle\Entity\Rate;
use Acme\MailBundle\Entity\Email;
use Acme\MailBundle\Form\Type\RegistrationType;
use Acme\MailBundle\Form\Type\UserType;
use Acme\MailBundle\Form\Model\Registration;
use Symfony\Component\Form\FormError;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpFoundation\RedirectResponse;

class AuthController extends Controller
{
    public function formAction(Request $request) {
        $user = new User;
        $form = $this->createForm(new UserType(), $user);
        $form->handleRequest($request);
        if ($form->isValid()){
            $response = new Response();
            $response->setStatusCode('200');
            return $response;
        }
        return $this->render('AcmeMailBundle:Templates:user_form.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    public function registrationAction(Request $request)
    {
        $registration = new Registration();
        $form = $this->createForm(new RegistrationType(), $registration);
        if ($request->getMethod() == 'POST') {
            $form->bind($request);
            $registration = $form->getData();
            $user = $registration->getUser();
            //$user->getSettings()->setBalanse(0);
            //$settings = $registration->getSettings();

            if ( $form->isValid() ) {
                $repository = $this->getDoctrine()
                    ->getRepository('AcmeMailBundle:User');
                $username = $user->getUsername();
                if (! $repository->findOneBy(array('username' => $username))){
                    $encoder = $this->get('security.encoder_factory')->getEncoder($user);
                    $password = $encoder->encodePassword($user->getPassword(), $user->getSalt());
                    if (!$user->getSettings()) $user->setSettings(new Settings());
                    $user->getSettings()->setUser($user);
                    $user -> setPassword($password);
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($user);
                    $em->flush();
                    return new RedirectResponse($this->generateUrl('_login'));
                }
                else
                {
                    $form -> addError(new FormError("Email not unique"));
                    return $this->render('AcmeMailBundle:Auth:registration.html.twig', array(
                    'form' => $form->createView()));
                }
            } else return $this->render('AcmeMailBundle:Auth:registration.html.twig', array(
                'form' => $form->createView(),
            ));
        } else {
            return $this->render('AcmeMailBundle:Auth:registration.html.twig', array(
                'form' => $form->createView(),
            ));
        }
    }

    public function loginAction (Request $request)
    {
        $session = $request->getSession();
        $message = "";
        // get the login error if there is one
        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(
                SecurityContext::AUTHENTICATION_ERROR
            );

        } else if (null !== $session && $session->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
            $message = "Проверьте логин и пароль";
        } else {
            $error = '';
        }

        if ($error) {
            $error = $error->getMessage(); // WARNING! Symfony source code identifies this line as a potential security threat.
        }

        $lastUsername = (null === $session) ? '' : $session->get(SecurityContext::LAST_USERNAME);

        return $this->render(
            'AcmeMailBundle:Auth:login.html.twig',
            array(
                // last username entered by the user
                'username' =>$lastUsername,
                'error'         => $error,
                'message' =>$message
            )
        );
    }

    public function loginCheckAction(Request $request)
    {}

    public function logoutAction(Request $request)
    {
        $session = $request->getSession();
        $session->clear();
        return new RedirectResponse($this->generateUrl('_hello'));
    }

    public function saveUser($user)
    {
        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();

        return new Response("new user saved, id=".$user->getId());
    }

    public function passwordAction(Request $request) {
        $user = $this->getUser();
        $message = "";
        $pass_form = $this->createFormBuilder()
            ->setAction($this->generateUrl('_profile_password'))
            ->setMethod('POST')
            ->add('password', 'password', array('required' => true,
                'label' => "Старый пароль"
            ))
            ->add('new_password', 'repeated', array(
                'first_name'  => 'password',
                'second_name' => 'confirm',
                'type'        => 'password',
                'first_options'  => array('label' => 'Новый пароль'),
                'second_options' => array('label' => 'Подтверждение пароля'),
            ))
            ->add ('password_submit', 'submit', array('label' => 'Сохранить'))
            ->getForm();

        if ($request->getMethod() == 'POST') {
            $pass_form ->handleRequest($request);
            if($pass_form->isValid()) {
                $password = $pass_form->get('password')->getData();
                $new_password = $pass_form->get('new_password')->getData();
                $encoder = $this->get('security.encoder_factory')->getEncoder($user);
                $password = $encoder->encodePassword($password, $user->getSalt());
                if($user->getPassword() === $password ){
                    $new_password = $encoder->encodePassword($new_password, $user->getSalt());
                    $user -> setPassword($new_password);
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($user);
                    $em->flush();
                    return new RedirectResponse($this->generateUrl('_profile'));
                } else $message = "Неправильный старый пароль";
            }
        }

        return $this->render('AcmeMailBundle:Auth:password.html.twig', array (
            'password_form' => $pass_form->createView(),
            'message' => $message));
    }
    
}