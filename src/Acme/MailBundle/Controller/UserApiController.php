<?php
namespace Acme\MailBundle\Controller;

use Acme\MailBundle\Entity\User,
    Acme\MailBundle\Form\Type\UserType;


use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\FormError;

use FOS\RestBundle\View\View,
    FOS\RestBundle\View\ViewHandler,
    FOS\RestBundle\View\RouteRedirectView;
use FOS\RestBundle\Controller\FOSRestController;

use JMS\Serializer\SerializationContext,
    JMS\Serializer\SerializerBuilder;

class UserAPIController extends FOSRestController {

    public function newAction() {
        return $this->processForm(new User());
        }

    public function editAction(User $user)
    {
        return $this->processForm($user);
    }

    private function processForm(User $user)
    {
        //return data:
        $data = "";
        $statusCode = $statusCode = $this->isNew($user)? 201 : 204;

        $form = $this->createForm(new UserType(), $user);
        $form->handleRequest($this->getRequest());
        if ($form->isValid()) {
            //save user
            $repository = $this->getDoctrine()
                ->getRepository('AcmeMailBundle:User');

            $encoder = $this->get('security.encoder_factory')->getEncoder($user);
            $password = $encoder->encodePassword($user->getPassword(), $user->getSalt());
            $user -> setPassword($password);
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            $data = 'success: user saved';
        } else {
            $data = $form;
            $statusCode = 400;
        }

        $view = $this->view($data, $statusCode)
            ->setFormat('json');
        if (201 === $statusCode) {
            $view->setHeader('Location',
                $this->generateUrl(
                    '_api_users_get', array('id' => $user->getId()),
                    true // absolute
                )
            );
        }
        return $this->handleView($view);
    }

    /* проверка - новый пользователь или нет
     */
    private function isNew(User $user) {

        if (is_null($user->getId()) or $user->getId() == 0) return true;
            else return false;
        //if($repository->findOneBy(array('username' => $username))) return true;
        //    else return false;
    }

    public function getAction($id)
    {
        $user = $this->getDoctrine()
            ->getRepository('AcmeMailBundle:User')
            ->find($id);
        if (!$user) {
            throw $this->createNotFoundException(
                'No user found for id ' . $id
            );
        }

        $view = $this->view($user, 200)
            ->setTemplateVar('user')
            ->setFormat('json');

        return $this->handleView($view);
    }

    public function allAction()
    {
        $users = $this->getDoctrine()
            ->getRepository('AcmeMailBundle:User')
            ->findAll();

        if (!$users) {
            throw $this->createNotFoundException(
                'No users found '
            );
        }

        $view = $this->view($users, 200)
            ->setTemplateVar('user')
            ->setFormat('json');

        return $this->handleView($view);
    }

    public function removeAction($id)
    {

        $user = $this->getDoctrine()
            ->getRepository('AcmeMailBundle:User')
            ->find($id);

        if (!$user) {
            $view = $this->view("error: user not found", 404)
                ->setTemplateVar('message')
                ->setFormat('json');
        } else {
            $em = $this->getDoctrine()->getManager();
            $em->remove($user);
            $em->flush();

            $view = $this->view("success: user deleted", 200)
                ->setTemplateVar('message')
                ->setFormat('json');
        }
        return $this->handleView($view);
    }

    public function getMailingsAction($user_id)
    {
        $user = $this->getDoctrine()
            ->getRepository('AcmeMailBundle:User')
            ->find($user_id);
        if (!$user) throw $this->createNotFoundException('user not found');

        $mailings = $user->getMailings();
        if (!$mailings) throw $this->createNotFoundException('not found');

        $view = $this->view($mailings, 200)
            ->setTemplateVar('mailings')
            ->setFormat('json');
        return $this->handleView($view);
    }

    public function getTemplatesAction($user_id)
    {
        $user = $this->getDoctrine()
            ->getRepository('AcmeMailBundle:User')
            ->find($user_id);
        if (!$user) throw $this->createNotFoundException('user not found');

        $templates = $user->getTemplates();
        if (!$templates) throw $this->createNotFoundException('not found');

        $view = $this->view($templates, 200)
            ->setTemplateVar('templates')
            ->setFormat('json');
        return $this->handleView($view);
    }

    public function getMailListsAction($user_id){
        $user = $this->getDoctrine()
            ->getRepository('AcmeMailBundle:User')
            ->find($user_id);
        if (!$user) throw $this->createNotFoundException('user not found');

        $lists = $user->getMaillists();
        if (!$lists) throw $this->createNotFoundException('not found');

        $view = $this->view($lists, 200)
            ->setTemplateVar('lists')
            ->setFormat('json');
        return $this->handleView($view);
    }
}