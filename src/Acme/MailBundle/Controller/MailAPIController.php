<?php
namespace Acme\MailBundle\Controller;

use Acme\MailBundle\Entity\User,
    Acme\MailBundle\Entity\Mailing,
    Acme\MailBundle\Entity\MailList,
    Acme\MailBundle\Entity\Target,
    Acme\MailBundle\Entity\Message,
    Acme\MailBundle\Entity\SimpleMessage,
    Acme\MailBundle\Form\Type\MessageAPIType;

use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\Common\Collections\ArrayCollection;

use FOS\RestBundle\View\View,
    FOS\RestBundle\View\ViewHandler,
    FOS\RestBundle\View\RouteRedirectView;
use FOS\RestBundle\Controller\FOSRestController;

use JMS\Serializer\SerializationContext,
    JMS\Serializer\SerializerBuilder;

class MailAPIController extends FOSRestController
{
    public function getMessageAction($id)
    {
        $message = $this->getDoctrine()
            ->getRepository('AcmeMailBundle:Message')
            ->find($id);
        if (!$message) throw $this->createNotFoundException('not found');

        $view = $this->view($message, 200)
            ->setTemplateVar('message')
            ->setFormat('json');
        return $this->handleView($view);
    }

    public function getMailMessageAction($mail_id)
    {
        $messages = $this->getDoctrine()
            ->getRepository('AcmeMailBundle:Message')
            ->findBy(array('mailing' => $mail_id));
        if (!$messages) throw $this->createNotFoundException('not found');

        $view = $this->view($messages, 200)
            ->setTemplateVar('messages')
            ->setFormat('json');
        return $this->handleView($view);
    }

    public function getMessageAllAction()
    {
        $messages = $this->getDoctrine()
            ->getRepository('AcmeMailBundle:Message')
            ->findAll();
        if (!$messages) throw $this->createNotFoundException('not found');

        $view = $this->view($messages, 200)
            ->setTemplateVar('messages')
            ->setFormat('json');
        return $this->handleView($view);
    }

    public function getMailingAction($id)
    {
        $mailing = $this->getDoctrine()
            ->getRepository('AcmeMailBundle:Mailing')
            ->find($id);
        if (!$mailing) throw $this->createNotFoundException('not found');

        $view = $this->view($mailing, 200)
            ->setTemplateVar('mailing')
            ->setFormat('json');
        return $this->handleView($view);
    }

    public function getMailingViewAction($id)
    {
        $mailing = $this->getDoctrine()
            ->getRepository('AcmeMailBundle:Mailing')
            ->find($id);
        if (!$mailing) throw $this->createNotFoundException('not found');

        $view = $this->view($mailing, 200)
            ->setTemplate('AcmeMailBundle:Mail:mail_stat.html.twig')
            ->setTemplateVar('mail')
            ->setFormat('html');
        return $this->handleView($view);
    }

    public function getMailingAllAction()
    {
        $mailings = $this->getDoctrine()
            ->getRepository('AcmeMailBundle:Mailing')
            ->findAll();
        if (!$mailings) throw $this->createNotFoundException('not found');

        $view = $this->view($mailings, 200)
            ->setTemplateVar('mailings')
            ->setFormat('json');
        return $this->handleView($view);
    }

    public function resendMailAction()
    {
        $user = $this->get('security.context')->getToken()->getUser();
        date_default_timezone_set('Asia/Irkutsk');
        $now = new \DateTime("now"); //date('Y-m-d H-i', time());

        $request = $this->get('request');
        $id = $request->request->get('id');
        if ($id != 0) {
            $date = $request->request->get('date');
            $date = new \DateTime($date);
            if (is_null($date) or !isset($date)) $date = $now;
            $mailing = $this->getDoctrine()
                ->getRepository('AcmeMailBundle:Mailing')
                ->find($id);
            if (!$mailing) throw $this->createNotFoundException('not found');

            $status = 1;
            $count = $mailing->getMessages()->count();
            foreach ($mailing->getMessages() as $msg) {
                if (!is_null($user->getRate())) {
                    $balance = $user->getSettings()->getBalance();
                    $new_balance = $balance - ($count * $user->getRate()->getMessagePrice());
                    //$balance = $balance - ($mailing->getMessages()->count() * $msg->getPrice());
                    if ($balance <= 0 or $new_balance < 0) {
                        $mailing->setStatus(Mailing::$STATUS_NEED_PAY);
                        // TODO notice user
                    } else $user->getSettings()->setBalance($new_balance);
                } else {
                    $mailing->setStatus(Mailing::$STATUS_NEED_PAY);
                    // TODO notice user, redirect to rate page
                }
                $msg->setStatus($status);
                $msg->setDate($date);
            }
            $mailing->setStatus($status);
            $mailing->setDate($date);

            $em = $this->getDoctrine()->getManager();
            $em->persist($mailing);
            $em->flush();

            $view = $this->view($mailing, 200)
                ->setTemplate('AcmeMailBundle:Mail:mail_row.html.twig')
                ->setTemplateVar('mail')
                ->setFormat('html');
            return $this->handleView($view);

        }
        return new Response("Error: id is 0", Response::HTTP_BAD_REQUEST);
    }

    public function getQueueMailingAction($user_id){
        $user = $this->getDoctrine()
            ->getRepository('AcmeMailBundle:User')
            ->find($user_id);
        if (!$user) throw $this->createNotFoundException('user not found');

        date_default_timezone_set('Asia/Irkutsk');
        $now = new \DateTime("now"); //date('Y-m-d H-i', time());


        $mails = $user->getMailings();
        $result = new ArrayCollection();
        foreach ($mails as $mail) {
            $date = $mail->getDate();
            $status = $mail->getStatus();
            if ($date > $now and ($status == Mailing::$STATUS_NEW or $status=Mailing::$STATUS_PREPARE))
                $result[] = $mail;
        }

        if (!$result) throw $this->createNotFoundException('mails not found');

        $view = $this->view($result, 200)
            ->setTemplateVar('mailings')
            ->setFormat('json');
        return $this->handleView($view);
    }

    public function sendMailAction() {
        $message = new SimpleMessage();
        $form = $this->createForm(new MessageAPIType(), $message);
        $form->handleRequest($this->getRequest());
        if ($form->isValid()) {
            $sender_name = $form->get('sender')->getData();
            $sender = $this->getDoctrine()
                ->getRepository('AcmeMailBundle:Email')
                ->findOneBy(array('value' => $sender_name));

            $message->setSender($sender);
            $repository = $this->getDoctrine()
                ->getRepository('AcmeMailBundle:SimpleMessage');
            $em = $this->getDoctrine()->getManager();
            $em->persist($message);
            $em->flush();
            $statusCode = 201;
            $data = 'success: template saved';
        } else {
            $data = $form;
            $statusCode = 400;
        }
        $view = $this->view($data, $statusCode)
            ->setFormat('json');
        return $this->handleView($view);
    }

    public function getMailing() {

    }
}
