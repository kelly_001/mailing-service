<?php

namespace Acme\MailBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Doctrine\Common\Collections\ArrayCollection;

use Acme\MailBundle\Entity\Payment;
use Acme\MailBundle\Entity\PaymentOrder;
use Acme\MailBundle\Form\Type\PayType;

class PaymentController extends Controller
{
    public function PayAction(Request $request) {
        $user = $this->get('security.context')->getToken()->getUser();

        $form = $this->createForm(new PayType());
        $form -> handleRequest($request);

        if($form->isValid()) {
            $sum = $form->get('sum')->getData();
            $payment = $form->get('payment')->getData();
            $order = new PaymentOrder();
            $order->setSum($sum);
            date_default_timezone_set('Asia/Irkutsk');
            $now = new \DateTime("now");
            $order->setDate($now);
            $order->setStatus(PaymentOrder::$STATUS_NEW);
            $order->setUser($user);
                $user->addOrder($order);
            $em=$this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            return new RedirectResponse(
                $this->generateUrl("payment_".$payment->getName(), array('order_id' => $order->getId())));

            /*$user->getSettings()->setBalance($user->getSettings()->getBalance()+$sum);
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            return new RedirectResponse($this->generateUrl('_profile'));*/
        }

        return $this->render('AcmeMailBundle:Default:pay.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}