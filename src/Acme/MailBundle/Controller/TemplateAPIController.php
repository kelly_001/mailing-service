<?php
namespace Acme\MailBundle\Controller;

use Acme\MailBundle\Entity\Template,
    Acme\MailBundle\Form\Type\TemplateType;


use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\FormError;

use FOS\RestBundle\View\View,
    FOS\RestBundle\View\ViewHandler,
    FOS\RestBundle\View\RouteRedirectView;
use FOS\RestBundle\Controller\FOSRestController;

use JMS\Serializer\SerializationContext,
    JMS\Serializer\SerializerBuilder;

class TemplateAPIController extends FOSRestController {


    public function newAction(){
        return $this->processForm(new Template());
    }

    public function editAction(Template $template) {
        return $this->processForm($template);
    }

    private function processForm(Template $template)
    {
        //return data:
        $data = "";
        $statusCode = $statusCode = $this->isNew($template)? 201 : 204;

        $form = $this->createForm(new TemplateType(), $template);
        $form->handleRequest($this->getRequest());
        if ($form->isValid()) {
            $repository = $this->getDoctrine()
                ->getRepository('AcmeMailBundle:Template');
            $em = $this->getDoctrine()->getManager();
            $em->persist($template);
            $em->flush();
            $data = 'success: template saved';
        } else {
            $data = $form;
            $statusCode = 400;
        }

        $view = $this->view($data, $statusCode)
            ->setFormat('json');
        if (201 === $statusCode) {
            $view->setHeader('Location',
                $this->generateUrl(
                    '_api_templatess_get', array('id' => $template->getId()),
                    true // absolute
                )
            );
        }
        return $this->handleView($view);
    }

    private function isNew(Template $template) {

        if (is_null($template->getId()) or $template->getId() == 0) return true;
        else return false;
    }

    public function allAction()
    {
        $templates = $this->getDoctrine()
            ->getRepository('AcmeMailBundle:Template')
            ->findAll();

        if (!$templates) {
            throw $this->createNotFoundException(
                'No template found'
            );
        }

        $view = $this->view($templates, 200)
            ->setTemplateVar('template')
            ->setFormat('json');

        return $this->handleView($view);
    }


    public function getHTMLAction($id)
    {
        $template = $this->getDoctrine()
            ->getRepository('AcmeMailBundle:Template')
            ->find($id);

        if (!$template) {
            throw $this->createNotFoundException(
                'No template found'
            );
        }

        $form = $this->createForm(new TemplateType(), $template);

        $view = $this->view($form, 200)
            ->setTemplate('AcmeMailBundle:Template:form.html.twig')
            ->setTemplateVar('template')
            ->setFormat('html');

        return $this->handleView($view);
    }

    public function getJSONAction($id)
    {
        $logger = $this->get('logger');
        $template = $this->getDoctrine()
            ->getRepository('AcmeMailBundle:Template')
            ->find($id);

        if (!$template) {
            throw $this->createNotFoundException(
                'No template found'
            );
        }

        $view = $this->view($template, 200)
            ->setTemplateVar('template')
            ->setFormat('json');

        return $this->handleView($view);
    }

    public function removeAction($id)
    {
        $logger = $this->get('logger');
        //сначала ищем запись
        $template = $this->getDoctrine()
            ->getRepository('AcmeMailBundle:Template')
            ->find($id);
        //если нет, то кидаем исключение
        if (!$template) {
            $view = $this->view("error: template not found", 404)
                ->setTemplateVar('message')
                ->setFormat('json');
            //throw $this->createNotFoundException('No template found');}
        } else {
            $em = $this->getDoctrine()->getManager();
            $em->remove($template);
            $em->flush();

            $view = $this->view("success: template deleted", 200)
                ->setTemplateVar('message')
                ->setFormat('json');
            //return new Response("deleted", Response::HTTP_OK);
        }
        return $this->handleView($view);
    }
}