<?php
namespace Acme\MailBundle\Controller;

use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\FormError;

use FOS\RestBundle\View\View,
    FOS\RestBundle\View\ViewHandler,
    FOS\RestBundle\View\RouteRedirectView;
use FOS\RestBundle\Controller\FOSRestController;

use JMS\Serializer\SerializationContext,
    JMS\Serializer\SerializerBuilder;

use Acme\MailBundle\Entity\Rate;

class RateAPIController extends  FOSRestController{

    public function getAction($id)
    {
        $rate = $this->getDoctrine()
            ->getRepository('AcmeMailBundle:Rate')
            ->find($id);

        if (!$rate) {
            throw $this->createNotFoundException('rate not found');
        }
        $view = $this->view($rate, 200)
            ->setTemplateVar('rate')
            ->setFormat('json');

        //print_r($rate);
        return $this->handleView($view);
    }

    public function getHTMLAction($id)
    {
        $rate = $this->getDoctrine()
            ->getRepository('AcmeMailBundle:Rate')
            ->find($id);

        if (!$rate) {
            throw $this->createNotFoundException('rate not found');
        }

        $view = $this->view($rate, 200)
            ->setTemplate('AcmeMailBundle:Rate:template.html.twig')
            ->setTemplateVar('rate')
            ->setFormat('html');

        return $this->handleView($view);
    }

    public function allAction()
    {
        $rates = $this->getDoctrine()
            ->getRepository('AcmeMailBundle:Rate')
            ->findAll();
        if (!$rates) throw $this->createNotFoundException('rates not found at all');

        $view = $this->view($rates, 200)
            ->setTemplateVar('rate')
            ->setFormat('json');
        return $this->handleView($view);
    }
}