<?php
namespace Acme\MailBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Doctrine\Common\Collections\ArrayCollection;

use Acme\MailBundle\Entity\User,
    Acme\MailBundle\Entity\Template,
    Acme\MailBundle\Entity\Variable,
    Acme\MailBundle\Form\Type\TemplateType;

use Pagerfanta\Pagerfanta,
    Pagerfanta\Adapter\DoctrineCollectionAdapter;

class TemplateController extends Controller {

    private $default_vars = array("user", "user_email", "sender_email", "theme", "link");

    public function getDefaultVars() {
        return $this->default_vars;
    }

    public function templatesAction($page) {
        $templates = $this->getUser()->getTemplates();
        $adapter = new DoctrineCollectionAdapter($templates);
        $pager = new Pagerfanta($adapter);
        $pager->setMaxPerPage(5);

        try {
            $pager->setCurrentPage($page);

        } catch (NotValidCurrentPageException $e) {
            throw new NotFoundHttpException();
        }

        return $this->render('AcmeMailBundle:Template:templates.html.twig', array('pager' =>$pager));
    }

    /*
     * Создание и редактирование шаблонов
     * Использователь для форм ввода с текстом и списком переменных
     */
    public function editTemplateAction(Request $request, $id) {
        $user = $this->getUser();
        if ($id!=0) {
            $template = $this->getDoctrine()
                ->getRepository('AcmeMailBundle:Template')
                ->find($id);
        } else {
            $template = new Template();
        }

        $original = new ArrayCollection();
        foreach ($template->getVariables() as $var) {
            $original->add($var);
        }

        $form = $this->createForm(new TemplateType(), $template);
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            if ($form->isValid()) {
                if ($id == 0) {
                    $template->setUser($user);
                    $user->addTemplate($template);
                }
                $em = $this->getDoctrine()->getManager();
                foreach ($original as $var) {
                    if (false === $template->getVariables()->contains($var)) {
                        // remove the Task from the Tag
                        $template->getVariables()->removeElement($var);
                        // if it was a many-to-one relationship, remove the relationship like this
                        $var->setTemplate(null);
                        $em->remove($var);
                    }
                }
                foreach ($template->getVariables() as $var) {
                    $var->setTemplate($template);
                    $em->persist($var);
                }

                // проверка текста шаблона на неуказанные переменные и добавляем их
                $template = $this->parseTemplate($template);
                $em->persist($template);
                $em->persist($user);
                $em->flush();
                return new RedirectResponse($this->generateUrl('_templates'));
            } else {
                return $this->render('AcmeMailBundle:Template:template.html.twig', array(
                    'form' => $form->createView(),
                    'user' => $user,
                    'id' => $id,
                ));
            }
        } else {
            return $this->render('AcmeMailBundle:Template:template.html.twig', array(
                'form' => $form->createView(),
                'user' => $user,
                'id' => $id,
            ));
        }
    }

    /*
     * Удаляет шаблон по его id
     */
    public function removeTemplateAction(Request $request, $id) {
        $template = $this->getDoctrine()
            ->getRepository('AcmeMailBundle:Template')
            ->find($id);

        $user = $this->get('security.context')->getToken()->getUser();
        if(!$template || $template->getUser()->getId() != $user->getId())return new Response("wrong email id", 401);
        $em = $this->getDoctrine()->getManager();
        $em->remove($template);
        $em->flush();
        return new RedirectResponse($this->generateUrl('_profile'));
    }

    public function getUser()
    {
        return $this->get('security.context')->getToken()->getUser();
    }

    /*
     * Метод для проверки н неуказанные пользователем переменные твиг в шаблоне
     * Вызываеть каждый раз при создании и изменении шаблона
     * Чтобы шаблонизатор не выдавал ошибку на неопределеннве переменные при рендере
     */
    public function parseTemplate($template){
        $text = $template->getValue();
        // step 1; delete all "{}"
        $text = preg_replace(array('/\{\{/','/\}\}/'), '', $text);
        $template->setValue($text);
        // шаг 2: ищем переменные
        $matches = array();
        // массив с именами переменных шаблона
        $vars_value = array();

        // его заполнение переменными, что указал пользователь
        foreach ($template->getVariables() as $var) {
            $vars_value[] = $var->getName();
        }
        // шаблон: [[variable_name]]
        $pattern = "/\[\[\s*\w+\s*\]\]/";
        preg_match_all($pattern,$text,$matches);
        foreach ($matches[0] as $match) {
            preg_match('/[\w|\d]+/', $match, $match);
            if(array_search($match[0],$vars_value) === false) {
                $vars_value[]=$match[0];
                $new_variable = new Variable();
                $new_variable->setName($match[0]);
                if (array_search($match[0], $this->default_vars) !== false) $new_variable->setType(Variable::$TYPE_VAR);
                    else $new_variable->setType(Variable::$TYPE_TEXT);
                    $new_variable->setTemplate($template);
                $template->addVariable($new_variable);
            }
        }
        // return template with updated variables
        return $template;

    }

    /*
     * Метод для замены скобок переменных в шаблоне с [[]] на {{}}
     * Для рендера шаблона в твиг
     * возвращать текст
     */
    public function prerenderTemplate($text, $variables) {
        //$text = $template->getValue();
        //$variables = $template->getVariables();
        // step 1; delete all "{}"
        $text = preg_replace(array('/\{\{/','/\}\}/'), '', $text);
        //ste 2: replace vars
        foreach ($variables as $var) {
            $pattern1 = "/\[\[\s*".$var->getName()."\s*\]\]/";
            $pattern2 = "{{".$var->getName()."}}";
            $text = preg_replace($pattern1,$pattern2,$text);
        }
        return $text;
    }
}