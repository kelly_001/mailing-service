<?php

namespace Acme\MailBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Acme\MailBundle\Form\Type\RateType;
use Acme\MailBundle\Entity\Rate;

class RateController extends Controller
{
    public function editAction(Request $request, $id)
    {
        if ($id!=0) {
            $rate = $this->getDoctrine()
                ->getRepository('AcmeMailBundle:Rate')
                ->find($id);
        } else $rate = new Rate();
        $form = $this->createForm(new RateType(), $rate);

        if ($request->getMethod() == 'POST') {
            $form->bind($request);

            if ( $form->isValid() ) {
                //$rate = $form->getData()->getRate();
                $em = $this->getDoctrine()->getManager();
                $em->persist($rate);
                $em->flush();
                return $this->render('AcmeMailBundle:Default:index.html.twig');

            } else {
                return $this->render('AcmeMailBundle:Rate:manage.html.twig', array(
                    'form' => $form->createView(),
                    'id' => $id,
                ));
            }

        } else {
            return $this->render('AcmeMailBundle:Rate:manage.html.twig', array(
                'form' => $form->createView(),
                'id' =>$id,
            ));
        }
    }

    public function removeAction(Request $request, $id)
    {
        $rate = $this->getDoctrine()
            ->getRepository('AcmeMailBundle:Rate')
            ->find($id);

        if (!$rate) {
            return new Response("incorrect id", 401);
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($rate);
        $em->flush();
        return new RedirectResponse($this->generateUrl('_main_page'));
    }
}