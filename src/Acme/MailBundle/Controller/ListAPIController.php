<?php
namespace Acme\MailBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use FOS\RestBundle\View\View,
    FOS\RestBundle\View\ViewHandler,
    FOS\RestBundle\View\RouteRedirectView,
    FOS\RestBundle\Controller\FOSRestController;

use JMS\Serializer\SerializationContext,
    JMS\Serializer\SerializerBuilder;

use Acme\MailBundle\Entity\Target,
    Acme\MailBundle\Entity\MailList,
    Acme\MailBundle\Entity\User;

class ListAPIController extends FOSRestController
{
    public function removeTargetAction($id)
    {
        $target = $this->getDoctrine()
            ->getRepository('AcmeMailBundle:Target')
            ->find($id);

        if (!$target) {
            $view = $this->view("error: entity not found", 404)
                ->setTemplateVar('message')
                ->setFormat('json');
        } else {
            $em = $this->getDoctrine()->getManager();
            $em->remove($target);
            $em->flush();

            $view = $this->view("success: entity deleted", 200)
                ->setTemplateVar('message')
                ->setFormat('json');
        }
        return $this->handleView($view);
    }

    public function getTargetAction($id)
    {
        $target = $this->getDoctrine()
            ->getRepository('AcmeMailBundle:Target')
            ->find($id);
        if (!$target) {
            throw $this->createNotFoundException(
                'No target found'
            );
        }

        $view = $this->view($target, 200)
            ->setTemplateVar('target')
            ->setFormat('json');

        return $this->handleView($view);
    }

    /*
     * получение всех адресов из списка рассылки по id рассылки
     * ( $page - страница таблицы, для пагинации во вью)
     */
    public function getListTargetsAction($id) //, $page)
    {
        $targets = $this->getDoctrine()
            ->getRepository('AcmeMailBundle:Target')
            ->findBy(array('maillist'=>$id));
        if (!$targets) throw $this->createNotFoundException(
            'Not found'
        );

        $view = $this->view($targets, 200)
            ->setTemplateVar('targets')
            ->setFormat('json');
        return $this->handleView($view);
    }

    /*
     * получение списка по номеру
     */
    public function getAction($id)
    {
        $list = $this->getDoctrine()
            ->getRepository('AcmeMailBundle:MailList')
            ->find($id);
        if (!$list) {
            throw $this->createNotFoundException(
                'Entity not found'
            );
        }

        $view = $this->view($list, 200)
            ->setTemplateVar('list')
            ->setFormat('json');

        return $this->handleView($view);
    }

    public function removeAction($id)
    {
        $list = $this->getDoctrine()
            ->getRepository('AcmeMailBundle:MailList')
            ->find($id);

        if (!$list) {
            $view = $this->view("error: mailing list not found", 404)
                ->setTemplateVar('message')
                ->setFormat('json');
        } else {
            $em = $this->getDoctrine()->getManager();
            $em->remove($list);
            $em->flush();

            $view = $this->view("success: mailing list deleted", 200)
                ->setTemplateVar('message')
                ->setFormat('json');
        }
        return $this->handleView($view);
    }

}