<?php
namespace Acme\MailBundle\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

class Mailing
{
    private $id;
    private $status;
    private $theme;
    private $date;
    private $messages;
    private $group;

    public static $STATUS_NEW = 1;
    public static $STATUS_PREPARE = 2;
    public static $STATUS_SENT = 3;
    public static $STATUS_ERROR = 4;
    public static $STATUS_NEED_PAY = 5;
    public static $STATUS_CANCELLED = 6;
    public static $STATUS_DOSTAVLENO = 7;

    public function _construct()
    {
        $this->messages = new ArrayCollection();
    }
    /**
     * @var \Acme\MailBundle\Entity\Email
     */
    private $sender;

    /**
     * @var \Acme\MailBundle\Entity\User
     */
    private $user;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->messages = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set id
     *
     * @param integer $id
     * @return Mailing
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Mailing
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set theme
     *
     * @param string $theme
     * @return Mailing
     */
    public function setTheme($theme)
    {
        $this->theme = $theme;

        return $this;
    }

    /**
     * Get theme
     *
     * @return string 
     */
    public function getTheme()
    {
        return $this->theme;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Mailing
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Add messages
     *
     * @param \Acme\MailBundle\Entity\Message $messages
     * @return Mailing
     */
    public function addMessage(\Acme\MailBundle\Entity\Message $messages)
    {
        $this->messages[] = $messages;

        return $this;
    }

    /**
     * Remove messages
     *
     * @param \Acme\MailBundle\Entity\Message $messages
     */
    public function removeMessage(\Acme\MailBundle\Entity\Message $messages)
    {
        $this->messages->removeElement($messages);
    }

    /**
     * Get messages
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * Set user
     *
     * @param \Acme\MailBundle\Entity\User $user
     * @return Mailing
     */
    public function setUser(\Acme\MailBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Acme\MailBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set group
     *
     * @param \Acme\MailBundle\Entity\MailList $group
     * @return Mailing
     */
    public function setGroup(\Acme\MailBundle\Entity\MailList $group = null)
    {
        $this->group = $group;
    
        return $this;
    }

    /**
     * Get group
     *
     * @return \Acme\MailBundle\Entity\MailList 
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * Set sender
     *
     * @param \Acme\MailBundle\Entity\Email $sender
     * @return Mailing
     */
    public function setSender(\Acme\MailBundle\Entity\Email $sender = null)
    {
        $this->sender = $sender;
    
        return $this;
    }

    /**
     * Get sender
     *
     * @return \Acme\MailBundle\Entity\Email 
     */
    public function getSender()
    {
        return $this->sender;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $attachments;


    /**
     * Add attachments
     *
     * @param \Acme\MailBundle\Entity\Attachment $attachments
     * @return Mailing
     */
    public function addAttachment(\Acme\MailBundle\Entity\Attachment $attachments)
    {
        $this->attachments[] = $attachments;
    
        return $this;
    }

    /**
     * Remove attachments
     *
     * @param \Acme\MailBundle\Entity\Attachment $attachments
     */
    public function removeAttachment(\Acme\MailBundle\Entity\Attachment $attachments)
    {
        $this->attachments->removeElement($attachments);
    }

    /**
     * Get attachments
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAttachments()
    {
        return $this->attachments;
    }
}
