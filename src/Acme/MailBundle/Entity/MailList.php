<?php

namespace Acme\MailBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

class MailList
{
    protected $id;
    protected $name;
    protected $targets;
    protected $mailings;
    public function _construct()
    {
        $this->targets = new ArrayCollection();
        $this->mailings = new \Doctrine\Common\Collections\ArrayCollection();
    }
    /**
     * @var \Acme\MailBundle\Entity\User
     */
    protected $user;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->targets = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set id
     *
     * @param integer $id
     * @return MailList
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return MailList
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add targets
     *
     * @param \Acme\MailBundle\Entity\Target $targets
     * @return MailList
     */
    public function addTarget(\Acme\MailBundle\Entity\Target $targets)
    {
        $this->targets[] = $targets;

        return $this;
    }

    /**
     * Remove targets
     *
     * @param \Acme\MailBundle\Entity\Target $targets
     */
    public function removeTarget(\Acme\MailBundle\Entity\Target $targets)
    {
        $this->targets->removeElement($targets);
    }

    /**
     * Get targets
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTargets()
    {
        return $this->targets;
    }

    /**
     * Set user
     *
     * @param \Acme\MailBundle\Entity\User $user
     * @return MailList
     */
    public function setUser(\Acme\MailBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Acme\MailBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add mailings
     *
     * @param \Acme\MailBundle\Entity\Mailing $mailings
     * @return MailList
     */
    public function addMailing(\Acme\MailBundle\Entity\Mailing $mailings)
    {
        $this->mailings[] = $mailings;
    
        return $this;
    }

    /**
     * Remove mailings
     *
     * @param \Acme\MailBundle\Entity\Mailing $mailings
     */
    public function removeMailing(\Acme\MailBundle\Entity\Mailing $mailings)
    {
        $this->mailings->removeElement($mailings);
    }

    /**
     * Get mailings
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMailings()
    {
        return $this->mailings;
    }
}
