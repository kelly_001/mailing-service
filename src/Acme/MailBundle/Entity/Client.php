<?php
// src/Acme/MailBundle/Entity/Client.php

namespace Acme\MailBundle\Entity;

use FOS\OAuthServerBundle\Entity\Client as BaseClient;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Client extends BaseClient
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    public function __construct()
    {
        parent::__construct();
        // your own logic
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $access_tokens;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $auth_codes;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $refresh_tokens;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add access_tokens
     *
     * @param \Acme\MailBundle\Entity\AccessToken $accessTokens
     * @return Client
     */
    public function addAccessToken(\Acme\MailBundle\Entity\AccessToken $accessTokens)
    {
        $this->access_tokens[] = $accessTokens;

        return $this;
    }

    /**
     * Remove access_tokens
     *
     * @param \Acme\MailBundle\Entity\AccessToken $accessTokens
     */
    public function removeAccessToken(\Acme\MailBundle\Entity\AccessToken $accessTokens)
    {
        $this->access_tokens->removeElement($accessTokens);
    }

    /**
     * Get access_tokens
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAccessTokens()
    {
        return $this->access_tokens;
    }

    /**
     * Add auth_codes
     *
     * @param \Acme\MailBundle\Entity\AuthCode $authCodes
     * @return Client
     */
    public function addAuthCode(\Acme\MailBundle\Entity\AuthCode $authCodes)
    {
        $this->auth_codes[] = $authCodes;

        return $this;
    }

    /**
     * Remove auth_codes
     *
     * @param \Acme\MailBundle\Entity\AuthCode $authCodes
     */
    public function removeAuthCode(\Acme\MailBundle\Entity\AuthCode $authCodes)
    {
        $this->auth_codes->removeElement($authCodes);
    }

    /**
     * Get auth_codes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAuthCodes()
    {
        return $this->auth_codes;
    }

    /**
     * Add refresh_tokens
     *
     * @param \Acme\MailBundle\Entity\RefreshToken $refreshTokens
     * @return Client
     */
    public function addRefreshToken(\Acme\MailBundle\Entity\RefreshToken $refreshTokens)
    {
        $this->refresh_tokens[] = $refreshTokens;

        return $this;
    }

    /**
     * Remove refresh_tokens
     *
     * @param \Acme\MailBundle\Entity\RefreshToken $refreshTokens
     */
    public function removeRefreshToken(\Acme\MailBundle\Entity\RefreshToken $refreshTokens)
    {
        $this->refresh_tokens->removeElement($refreshTokens);
    }

    /**
     * Get refresh_tokens
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRefreshTokens()
    {
        return $this->refresh_tokens;
    }
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $password;


    /**
     * Set name
     *
     * @param string $name
     * @return Client
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }


}
