<?php

namespace Acme\MailBundle\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\ORM\Mapping as ORM;

class User implements UserInterface, \Serializable
{
    protected $id;
    protected $password;
    protected $emails;
    protected $templates;
    protected $rate;
    protected $settings;
    protected $mailings;
    private $isActive;
    protected $username;
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $maillists;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $orders;
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $messages;



    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Constructor
     */
    public function _construct()
    {
        $this->templates = new ArrayCollection();
        $this->emails = new ArrayCollection();
        $this->maillists = new ArrayCollection();
        $this->mailings = new ArrayCollection();
        $this->orders = new ArrayCollection();
        $this->messages = new ArrayCollection();
        $this->isActive = false;
        //$this->settings->setUser($this);
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set login
     *
     * @param string $login
     * @return User
     */

    /**
     * Set password
     *
     * @param string $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }
    /**
     * Set settings
     *
     * @param \Acme\MailBundle\Entity\Settings $settings
     * @return User
     */
    public function setSettings(\Acme\MailBundle\Entity\Settings $settings = null)
    {
        $this->settings = $settings;

        return $this;
    }

    /**
     * Get settings
     *
     * @return \Acme\MailBundle\Entity\Settings 
     */
    public function getSettings()
    {
        return $this->settings;
    }



    /**
     * Set rate
     *
     * @param \Acme\MailBundle\Entity\Rate $rate
     * @return User
     */
    public function setRate(\Acme\MailBundle\Entity\Rate $rate = null)
    {
        $this->rate = $rate;

        return $this;
    }

    /**
     * Get rate
     *
     * @return \Acme\MailBundle\Entity\Rate 
     */
    public function getRate()
    {
        return $this->rate;
    }

    /**
     * Add templates
     *
     * @param \Acme\MailBundle\Entity\Template $templates
     * @return User
     */
    public function addTemplate(\Acme\MailBundle\Entity\Template $templates)
    {
        $this->templates[] = $templates;

        return $this;
    }

    /**
     * Remove templates
     *
     * @param \Acme\MailBundle\Entity\Template $templates
     */
    public function removeTemplate(\Acme\MailBundle\Entity\Template $templates)
    {
        $this->templates->removeElement($templates);
    }

    /**
     * Get templates
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTemplates()
    {
        return $this->templates;
    }

    /**
     * @inheritDoc
     */
    public function getRoles()
    {
        return array('ROLE_USER');
    }

    /**
     * @inheritDoc
     */
    public function getSalt()
    {
       return '';
    }


    /**
     * @inheritDoc
     */
    public function eraseCredentials()
    {
    }

    /**
     * @see \Serializable::serialize()
     */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->
        ));
    }

    /**
     * @see \Serializable::unserialize()
     */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt
            ) = unserialize($serialized);
    }

    /**
     * Set username
     *
     * @param string $username
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Add emails
     *
     * @param \Acme\MailBundle\Entity\Email $emails
     * @return User
     */
    public function addEmail(\Acme\MailBundle\Entity\Email $emails)
    {
        $this->emails[] = $emails;

        return $this;
    }

    /**
     * Remove emails
     *
     * @param \Acme\MailBundle\Entity\Email $emails
     */
    public function removeEmail(\Acme\MailBundle\Entity\Email $emails)
    {
        $this->emails->removeElement($emails);
    }

    /**
     * Get emails
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEmails()
    {
        return $this->emails;
    }

    /**
     * Add maillists
     *
     * @param \Acme\MailBundle\Entity\MailList $maillists
     * @return User
     */
    public function addMaillist(\Acme\MailBundle\Entity\MailList $maillists)
    {
        $this->maillists[] = $maillists;

        return $this;
    }

    /**
     * Remove maillists
     *
     * @param \Acme\MailBundle\Entity\MailList $maillists
     */
    public function removeMaillist(\Acme\MailBundle\Entity\MailList $maillists)
    {
        $this->maillists->removeElement($maillists);
    }

    /**
     * Get maillists
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMaillists()
    {
        return $this->maillists;
    }

    /**
     * Add mailings
     *
     * @param \Acme\MailBundle\Entity\Mailing $mailings
     * @return User
     */
    public function addMailing(\Acme\MailBundle\Entity\Mailing $mailings)
    {
        $this->mailings[] = $mailings;
    
        return $this;
    }

    /**
     * Remove mailings
     *
     * @param \Acme\MailBundle\Entity\Mailing $mailings
     */
    public function removeMailing(\Acme\MailBundle\Entity\Mailing $mailings)
    {
        $this->mailings->removeElement($mailings);
    }

    /**
     * Get mailings
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMailings()
    {
        return $this->mailings;
    }

    /**
     * Add orders
     *
     * @param \Acme\MailBundle\Entity\PaymentOrder $orders
     * @return User
     */
    public function addOrder(\Acme\MailBundle\Entity\PaymentOrder $orders)
    {
        $this->orders[] = $orders;
    
        return $this;
    }

    /**
     * Remove orders
     *
     * @param \Acme\MailBundle\Entity\PaymentOrder $orders
     */
    public function removeOrder(\Acme\MailBundle\Entity\PaymentOrder $orders)
    {
        $this->orders->removeElement($orders);
    }

    /**
     * Get orders
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getOrders()
    {
        return $this->orders;
    }

    /**
     * Add messages
     *
     * @param \Acme\MailBundle\Entity\SimpleMessage $messages
     * @return User
     */
    public function addMessage(\Acme\MailBundle\Entity\SimpleMessage $messages)
    {
        $this->messages[] = $messages;
    
        return $this;
    }

    /**
     * Remove messages
     *
     * @param \Acme\MailBundle\Entity\SimpleMessage $messages
     */
    public function removeMessage(\Acme\MailBundle\Entity\SimpleMessage $messages)
    {
        $this->messages->removeElement($messages);
    }

    /**
     * Get messages
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->emails = new \Doctrine\Common\Collections\ArrayCollection();
        $this->templates = new \Doctrine\Common\Collections\ArrayCollection();
        $this->maillists = new \Doctrine\Common\Collections\ArrayCollection();
        $this->mailings = new \Doctrine\Common\Collections\ArrayCollection();
        $this->orders = new \Doctrine\Common\Collections\ArrayCollection();
        $this->messages = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $access_tokens;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $auth_codes;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $refresh_tokens;


    /**
     * Add access_tokens
     *
     * @param \Acme\MailBundle\Entity\AccessToken $accessTokens
     * @return User
     */
    public function addAccessToken(\Acme\MailBundle\Entity\AccessToken $accessTokens)
    {
        $this->access_tokens[] = $accessTokens;

        return $this;
    }

    /**
     * Remove access_tokens
     *
     * @param \Acme\MailBundle\Entity\AccessToken $accessTokens
     */
    public function removeAccessToken(\Acme\MailBundle\Entity\AccessToken $accessTokens)
    {
        $this->access_tokens->removeElement($accessTokens);
    }

    /**
     * Get access_tokens
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAccessTokens()
    {
        return $this->access_tokens;
    }

    /**
     * Add auth_codes
     *
     * @param \Acme\MailBundle\Entity\AuthCode $authCodes
     * @return User
     */
    public function addAuthCode(\Acme\MailBundle\Entity\AuthCode $authCodes)
    {
        $this->auth_codes[] = $authCodes;

        return $this;
    }

    /**
     * Remove auth_codes
     *
     * @param \Acme\MailBundle\Entity\AuthCode $authCodes
     */
    public function removeAuthCode(\Acme\MailBundle\Entity\AuthCode $authCodes)
    {
        $this->auth_codes->removeElement($authCodes);
    }

    /**
     * Get auth_codes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAuthCodes()
    {
        return $this->auth_codes;
    }

    /**
     * Add refresh_tokens
     *
     * @param \Acme\MailBundle\Entity\RefreshToken $refreshTokens
     * @return User
     */
    public function addRefreshToken(\Acme\MailBundle\Entity\RefreshToken $refreshTokens)
    {
        $this->refresh_tokens[] = $refreshTokens;

        return $this;
    }

    /**
     * Remove refresh_tokens
     *
     * @param \Acme\MailBundle\Entity\RefreshToken $refreshTokens
     */
    public function removeRefreshToken(\Acme\MailBundle\Entity\RefreshToken $refreshTokens)
    {
        $this->refresh_tokens->removeElement($refreshTokens);
    }

    /**
     * Get refresh_tokens
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRefreshTokens()
    {
        return $this->refresh_tokens;
    }
}
