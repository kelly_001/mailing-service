<?php

namespace Acme\MailBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SimpleMessage
 */
class SimpleMessage
{
    public static $STATUS_NEW = 1;
    public static $STATUS_PREPARE = 2;
    public static $STATUS_SENT = 3;
    public static $STATUS_ERROR = 4;
    public static $STATUS_NEED_PAY = 5;
    public static $STATUS_CANCELLED = 6;
    public static $STATUS_VIEWED = 7;

    /**
     * @var integer
     */
    private $id;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @var string
     */
    private $text;

    /**
     * @var string
     */
    private $target;

    /**
     * @var integer
     */
    private $status;

    /**
     * @var string
     */
    private $theme;

    /**
     * @var \Acme\MailBundle\Entity\User
     */
    private $user;

    /**
     * @var \Acme\MailBundle\Entity\Email
     */
    private $sender;


    /**
     * Set text
     *
     * @param string $text
     * @return SimpleMessage
     */
    public function setText($text)
    {
        $this->text = $text;
    
        return $this;
    }

    /**
     * Get text
     *
     * @return string 
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set target
     *
     * @param string $target
     * @return SimpleMessage
     */
    public function setTarget($target)
    {
        $this->target = $target;
    
        return $this;
    }

    /**
     * Get target
     *
     * @return string 
     */
    public function getTarget()
    {
        return $this->target;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return SimpleMessage
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set theme
     *
     * @param string $theme
     * @return SimpleMessage
     */
    public function setTheme($theme)
    {
        $this->theme = $theme;
    
        return $this;
    }

    /**
     * Get theme
     *
     * @return string 
     */
    public function getTheme()
    {
        return $this->theme;
    }

    /**
     * Set user
     *
     * @param \Acme\MailBundle\Entity\User $user
     * @return SimpleMessage
     */
    public function setUser(\Acme\MailBundle\Entity\User $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \Acme\MailBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set sender
     *
     * @param \Acme\MailBundle\Entity\Email $sender
     * @return SimpleMessage
     */
    public function setSender(\Acme\MailBundle\Entity\Email $sender = null)
    {
        $this->sender = $sender;
    
        return $this;
    }

    /**
     * Get sender
     *
     * @return \Acme\MailBundle\Entity\Email 
     */
    public function getSender()
    {
        return $this->sender;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $attachments;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->attachments = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add attachments
     *
     * @param \Acme\MailBundle\Entity\Attachment $attachments
     * @return SimpleMessage
     */
    public function addAttachment(\Acme\MailBundle\Entity\Attachment $attachments)
    {
        $this->attachments[] = $attachments;
    
        return $this;
    }

    /**
     * Remove attachments
     *
     * @param \Acme\MailBundle\Entity\Attachment $attachments
     */
    public function removeAttachment(\Acme\MailBundle\Entity\Attachment $attachments)
    {
        $this->attachments->removeElement($attachments);
    }

    /**
     * Get attachments
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAttachments()
    {
        return $this->attachments;
    }
}
