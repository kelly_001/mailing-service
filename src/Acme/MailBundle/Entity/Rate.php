<?php
namespace Acme\MailBundle\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

class Rate
{
    protected $id;
    protected $name;
    protected $description;
    protected $price;
    protected $message_price;
    protected $message_count;
    protected $users;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->users = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Rate
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set price
     *
     * @param integer $price
     * @return Rate
     */
    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    /**
     * Get price
     *
     * @return integer 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set message_price
     *
     * @param integer $messagePrice
     * @return Rate
     */
    public function setMessagePrice($messagePrice)
    {
        $this->message_price = $messagePrice;
        return $this;
    }

    /**
     * Get message_price
     *
     * @return integer 
     */
    public function getMessagePrice()
    {
        return $this->message_price;
    }

    /**
     * Set message_count
     *
     * @param integer $messageCount
     * @return Rate
     */
    public function setMessageCount($messageCount)
    {
        $this->message_count = $messageCount;

        return $this;
    }

    /**
     * Get message_count
     *
     * @return integer 
     */
    public function getMessageCount()
    {
        return $this->message_count;
    }
    /**
     * Add users
     *
     * @param \Acme\MailBundle\Entity\User $users
     * @return Rate
     */
    public function addUser(\Acme\MailBundle\Entity\User $users)
    {
        $this->users[] = $users;

        return $this;
    }

    /**
     * Remove users
     *
     * @param \Acme\MailBundle\Entity\User $users
     */
    public function removeUser(\Acme\MailBundle\Entity\User $users)
    {
        $this->users->removeElement($users);
    }

    public function removeAllUsers()
    {
        $this->users = null;
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Rate
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }
}
