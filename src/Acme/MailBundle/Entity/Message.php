<?php
namespace Acme\MailBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

class Message
{
    private $id;
    private $date;
    private $price = 0;
    private $status;
    private $text;
    /**
     * @var \Acme\MailBundle\Entity\Mailing
     */
    private $mailing;
    /**
     * @var \Acme\MailBundle\Entity\Target
     */
    private $target;


    public static $STATUS_NEW = 1;
    public static $STATUS_PREPARE = 2;
    public static $STATUS_SENT = 3;
    public static $STATUS_ERROR = 4;
    public static $STATUS_NEED_PAY = 5;
    public static $STATUS_CANCELLED = 6;
    public static $STATUS_VIEWED = 7;






    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Message
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Message
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set price
     *
     * @param string $price
     * @return Message
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set mailing
     *
     * @param \Acme\MailBundle\Entity\Mailing $mailing
     * @return Message
     */
    public function setMailing(\Acme\MailBundle\Entity\Mailing $mailing = null)
    {
        $this->mailing = $mailing;

        return $this;
    }

    /**
     * Get mailing
     *
     * @return \Acme\MailBundle\Entity\Mailing 
     */
    public function getMailing()
    {
        return $this->mailing;
    }

    /**
     * Set target
     *
     * @param \Acme\MailBundle\Entity\Target $target
     * @return Message
     */
    public function setTarget(\Acme\MailBundle\Entity\Target $target = null)
    {
        $this->target = $target;

        return $this;
    }

    /**
     * Get target
     *
     * @return \Acme\MailBundle\Entity\Target 
     */
    public function getTarget()
    {
        return $this->target;
    }

    /**
     * Set text
     *
     * @param string $text
     * @return Message
     */
    public function setText($text)
    {
        $this->text = $text;
    
        return $this;
    }

    /**
     * Get text
     *
     * @return string 
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @var \Acme\MailBundle\Entity\Email
     */

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $attachments;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->attachments = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add attachments
     *
     * @param \Acme\MailBundle\Entity\Attachment $attachments
     * @return Message
     */
    public function addAttachment(\Acme\MailBundle\Entity\Attachment $attachments)
    {
        $this->attachments[] = $attachments;
    
        return $this;
    }

    /**
     * Remove attachments
     *
     * @param \Acme\MailBundle\Entity\Attachment $attachments
     */
    public function removeAttachment(\Acme\MailBundle\Entity\Attachment $attachments)
    {
        $this->attachments->removeElement($attachments);
    }

    /**
     * Get attachments
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAttachments()
    {
        return $this->attachments;
    }
}
