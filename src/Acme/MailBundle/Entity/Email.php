<?php
namespace Acme\MailBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

class Email
{
    protected $id;
    protected $status;
    protected $value;
    protected $mailings;
    protected $user;

    public static $STATUS_NEW = 1; //новая
    public static $STATUS_CHECKED = 2; //подтверждена пользователем
    public static $STATUS_ACTIVE = 3; // проверена ауйтентификация и подтверждена клиентом
    public static $STATUS_ERROR = 4;

    /**
     * @inheritDoc
     */
    public function getSalt()
    {
        return '';
    }


    /**
     * Set id
     *
     * @param integer $id
     * @return Email
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Email
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set value
     *
     * @param string $value
     * @return Email
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string 
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set user
     *
     * @param \Acme\MailBundle\Entity\User $user
     * @return Email
     */
    public function setUser(\Acme\MailBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Acme\MailBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @var string
     */
    private $password;

    /**
     * @var \Acme\MailBundle\Entity\Transport
     */
    private $transport;


    /**
     * Set password
     *
     * @param string $password
     * @return Email
     */
    public function setPassword($password)
    {
        $this->password = $password;
    
        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set transport
     *
     * @param \Acme\MailBundle\Entity\Transport $transport
     * @return Email
     */
    public function setTransport(\Acme\MailBundle\Entity\Transport $transport = null)
    {
        $this->transport = $transport;
    
        return $this;
    }

    /**
     * Get transport
     *
     * @return \Acme\MailBundle\Entity\Transport 
     */
    public function getTransport()
    {
        return $this->transport;
    }

    /**
     * Add mailings
     *
     * @param \Acme\MailBundle\Entity\Mailing $mailings
     * @return Email
     */
    public function addMailing(\Acme\MailBundle\Entity\Mailing $mailings)
    {
        $this->mailings[] = $mailings;
    
        return $this;
    }

    /**
     * Remove mailings
     *
     * @param \Acme\MailBundle\Entity\Mailing $mailings
     */
    public function removeMailing(\Acme\MailBundle\Entity\Mailing $mailings)
    {
        $this->mailings->removeElement($mailings);
    }

    /**
     * Get mailings
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMailings()
    {
        return $this->mailings;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $messages;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->messages = new \Doctrine\Common\Collections\ArrayCollection();
        $this->mailings = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add messages
     *
     * @param \Acme\MailBundle\Entity\SimpleMessage $messages
     * @return Email
     */
    public function addMessage(\Acme\MailBundle\Entity\SimpleMessage $messages)
    {
        $this->messages[] = $messages;
    
        return $this;
    }

    /**
     * Remove messages
     *
     * @param \Acme\MailBundle\Entity\SimpleMessage $messages
     */
    public function removeMessage(\Acme\MailBundle\Entity\SimpleMessage $messages)
    {
        $this->messages->removeElement($messages);
    }

    /**
     * Get messages
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMessages()
    {
        return $this->messages;
    }
}
