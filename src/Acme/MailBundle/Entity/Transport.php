<?php

namespace Acme\MailBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Transport
 */
class Transport
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $host;

    /**
     * @var integer
     */
    private $port;

    /**
     * @var string
     */
    private $encryption;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $emails;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->emails = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set host
     *
     * @param string $host
     * @return Transport
     */
    public function setHost($host)
    {
        $this->host = $host;
    
        return $this;
    }

    /**
     * Get host
     *
     * @return string 
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * Set port
     *
     * @param integer $port
     * @return Transport
     */
    public function setPort($port)
    {
        $this->port = $port;
    
        return $this;
    }

    /**
     * Get port
     *
     * @return integer 
     */
    public function getPort()
    {
        return $this->port;
    }

    /**
     * Set encryption
     *
     * @param string $encryption
     * @return Transport
     */
    public function setEncryption($encryption)
    {
        $this->encryption = $encryption;
    
        return $this;
    }

    /**
     * Get encryption
     *
     * @return string 
     */
    public function getEncryption()
    {
        return $this->encryption;
    }

    /**
     * Add emails
     *
     * @param \Acme\MailBundle\Entity\Email $emails
     * @return Transport
     */
    public function addEmail(\Acme\MailBundle\Entity\Email $emails)
    {
        $this->emails[] = $emails;
    
        return $this;
    }

    /**
     * Remove emails
     *
     * @param \Acme\MailBundle\Entity\Email $emails
     */
    public function removeEmail(\Acme\MailBundle\Entity\Email $emails)
    {
        $this->emails->removeElement($emails);
    }

    /**
     * Get emails
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEmails()
    {
        return $this->emails;
    }
    /**
     * @var string
     */
    private $domains;


    /**
     * Set domains
     *
     * @param string $domains
     * @return Transport
     */
    public function setDomains($domains)
    {
        $this->domains = $domains;
    
        return $this;
    }

    /**
     * Get domains
     *
     * @return string 
     */
    public function getDomains()
    {
        return $this->domains;
    }
}
