<?php
namespace Acme\MailBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

class Template
{
    protected $id;
    protected $user;
    protected $name;
    protected $value;
    protected $variables;

    public static $TYPE_TEXT = 1;
    public static $TYPE_VAR = 2;
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Template
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set value
     *
     * @param string $value
     * @return Template
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string 
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set user
     *
     * @param \Acme\MailBundle\Entity\User $user
     * @return Template
     */
    public function setUser(\Acme\MailBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Acme\MailBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->variables = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add variables
     *
     * @param \Acme\MailBundle\Entity\Variable $variables
     * @return Template
     */
    public function addVariable(\Acme\MailBundle\Entity\Variable $variables)
    {
        $this->variables[] = $variables;
    
        return $this;
    }

    /**
     * Remove variables
     *
     * @param \Acme\MailBundle\Entity\Variable $variables
     */
    public function removeVariable(\Acme\MailBundle\Entity\Variable $variables)
    {
        $this->variables->removeElement($variables);
    }

    /**
     * Get variables
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getVariables()
    {
        return $this->variables;
    }
}
