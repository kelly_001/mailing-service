<?php
namespace Acme\MailBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

class Variable
{
    protected $id;
    protected $name;
    protected $type;
    protected $template;

    public static $TYPE_TEXT = 1;
    public static $TYPE_VAR = 2;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return Variable
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set template
     *
     * @param \Acme\MailBundle\Entity\Template $template
     * @return Variable
     */
    public function setTemplate(\Acme\MailBundle\Entity\Template $template = null)
    {
        $this->template = $template;
    
        return $this;
    }

    /**
     * Get template
     *
     * @return \Acme\MailBundle\Entity\Template 
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Variable
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
}
