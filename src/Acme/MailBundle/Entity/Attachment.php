<?php
namespace Acme\MailBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;


class Attachment {

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $path;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Attachment
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set path
     *
     * @param string $path
     * @return Attachment
     */
    public function setPath($path)
    {
        $this->path = $path;
    
        return $this;
    }

    /**
     * Get path
     *
     * @return string 
     */
    public function getPath()
    {
        return $this->path;
    }




    /**
     * @var \Acme\MailBundle\Entity\SimpleMessage
     */
    private $simple_message;


    /**
     * Set simple_message
     *
     * @param \Acme\MailBundle\Entity\SimpleMessage $simpleMessage
     * @return Attachment
     */
    public function setSimpleMessage(\Acme\MailBundle\Entity\SimpleMessage $simpleMessage = null)
    {
        $this->simple_message = $simpleMessage;
    
        return $this;
    }

    /**
     * Get simple_message
     *
     * @return \Acme\MailBundle\Entity\SimpleMessage 
     */
    public function getSimpleMessage()
    {
        return $this->simple_message;
    }
    /**
     * @var \Acme\MailBundle\Entity\Mailing
     */
    private $mailing;


    /**
     * Set mailing
     *
     * @param \Acme\MailBundle\Entity\Mailing $mailing
     * @return Attachment
     */
    public function setMailing(\Acme\MailBundle\Entity\Mailing $mailing = null)
    {
        $this->mailing = $mailing;
    
        return $this;
    }

    /**
     * Get mailing
     *
     * @return \Acme\MailBundle\Entity\Mailing 
     */
    public function getMailing()
    {
        return $this->mailing;
    }
}
