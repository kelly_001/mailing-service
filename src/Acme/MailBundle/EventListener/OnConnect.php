<?php //file src/Acme/MailBundle/EventListener/OnConnect.php
namespace Acme\MailBundle\EventListener;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\EntityManager;
class OnConnect {
    public function postConnect( $event ) 	{
        $conn = $event->getConnection();
        $conn->executeQuery("SET NAMES UTF8");
    }
}